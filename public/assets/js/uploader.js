$(function() {
    $('#picture').change(function() {
        
	$('.signin').hide();
	$('#profile_picture').html('<img src="/assets/images/ajaxloader.gif" width="128" height="128" >');
	
	var file_data = $("#picture").prop("files")[0];   // Getting the properties of file from file field
	var form_data = new FormData();                  // Creating object of FormData class
	form_data.append("ImageFile", file_data)              // Appending parameter named file with properties of file_field to form_data
	
	   
	   $.ajax({
			url: "/user/upload_profile_picture",
			dataType: 'script',
			cache: false,
            contentType: false,
			processData: false,
			data : form_data,
			type: "POST",
			success: function(data)
			{
				data = data.replace('"', '');
				$('#profile_picture').html('<img src="/assets/images/users/thumb_'+data+'" >');
				$('.signin').show();
			},
			error: function ()
			{
				$('#profile_picture').html('<span class="error" >Profile picture size should not be greater then 600*450 and nor be less then 150*150.</span>');
				$('.signin').show();
			}
		});
	   
	   
	 

    });
});