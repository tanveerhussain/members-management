<?php

class UserController extends \BaseController {

	/**
	 * Show the form for user to login
	 *
	 * @return Response
	 */
	public function login()
	{
		// Display user login form
		return View::make('login'); 
	}


	/**
	 * Validate user provided credential
	 *
	 * @return Response
	 */
	public function validateUser()
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'username'    => 'required', 
			'password' => 'required|alphaNum|min:3' 
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('user/login')
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {

			// create our user data for the authentication
			$userdata = array(
				'username' 	=> Input::get('username'),
				'password' 	=> Input::get('password'),
				'is_active' => 'y'
			);
			
			// attempt to do the login
			if (Auth::attempt($userdata)) {

				// validation successful!
				// redirect them to the secure rss listing page
				return Redirect::to('/');

			} else { 

				// validation not successful, send back to form
				return Redirect::to('user/login')
    		        ->with('flash_error', 'Your username/password combination was incorrect.')
	            	->withInput();
					

			}

		}
	}
	
	/**
	 * Show the form for user to sign up form
	 *
	 * @return Response
	 */
	public function sign_up()
	{
		// Display user sign up form
		return View::make('sign_up'); 
	}

	/**
	 * Validate user provided credential and register
	 *
	 * @return Response
	 */
	public function validateUserSignUp()
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'first_name'    => 'required', 
			'last_name'    => 'required', 
			'email_address'    => 'required|email|unique:users', 
			'username'    => 'required|unique:users', 
			'password' => 'required|alphaNum|min:3|Confirmed', 
			'password_confirmation' => 'required|alphaNum|min:3', 
			'term_and_condition'    => 'required'
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('user/sign_up')
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {
			
			$activation_token = md5(time());
			$token_expiry = date('Y-m-d');
			// create our user data for the register 
			$user = new User;
			$user->first_name       = Input::get('first_name');
			$user->last_name       = Input::get('last_name');
			$user->email_address      = Input::get('email_address');
			$user->username      = Input::get('username');			
			$user->password = Hash::make(Input::get('password'));
			$user->profile_type = 'public';
			$user->activation_token = $activation_token;
			$user->token_expiry = $token_expiry;
			$user->is_active = 'n';
			$user->save();			
			
			$lastInsertedId = $user->id;
			
			$is_found = DB::table('invitations')->where('email_address', '=', Input::get('email_address'))->get();
			if($is_found){
				$invitation = Invitation::find($is_found[0]->id);
				$invitation->is_register = 'y';
				$invitation->save();
				
				$friend = new Friend;
				$friend->user_id = $lastInsertedId;
			    $friend->invited_by = $is_found[0]->invited_by;
				$friend->save();
			}
			
			$is_found = DB::table('teaminvitations')->where('email_address', '=', Input::get('email_address'))->get();
			if($is_found){
				
				$team = Team::find($is_found[0]->team_id);
				$team_member = $team->team_member;
				if($team_member!=""){
					$team_member .= ','.$lastInsertedId;
				}else{
					$team_member = $lastInsertedId;
				}
				$team->team_member = $team_member;
				$team->save();
				
				Teaminvitation::where('id', '=', $is_found[0]->id)->delete();
				
			}
			
			
			//Send activation email
			$data['user'] = array('token' => $activation_token, 'first_name' => Input::get('first_name'), 'last_name' => Input::get('last_name'));
			Mail::send('emails.activation', $data, function($message) {
				$message->from('tanveer.hussain@nxb.com.pk', 'Tanveer Hussain ~ NXB');
				$message->to(Input::get('email_address'), Input::get('first_name').' '.Input::get('last_name'))->subject('Account activation - Laravel members app');
			});
			
			// go to thank you page
			return Redirect::to('user/thankyou')->with('flash_msg', 'You account has created, please check your inbox for account activation.');

		}
	}
	
	
	/**
	 * Validate user provided credential and activation
	 *
	 * @return Response
	 */
	public function activation()
	{	
		$users = DB::table('users')->where('is_active', '=', 'n')
						->Where(function($query)
						{
							$query->where('activation_token', '=', Request::segment(3))
								  ;
						})->get();
		if(empty($users)){
			return Redirect::to('user/thankyou')->with('error_msg', "Your token has been expired, please try again.");
		}else {
			User::where('activation_token', '=', Request::segment(3))->update(array('activation_token' => '', 'is_active' => 'y'));
			return Redirect::to('user/thankyou')->with('flash_msg', 'Your account has activated, please try login now.');
		}
		
	}
	
	/**
	 * Show the thank you page after sign up
	 *
	 * @return Response
	 */
	public function thankyou()
	{
		// Display user login form
		return View::make('thankyou'); 
	}
	
	/**
	 * Show the form for user to resend activation code
	 *
	 * @return Response
	 */
	public function resend_activation_code()
	{
		// Display user resend activation code form
		return View::make('resend_activation_code'); 
	}
	
	/**
	 * Validate user provided credential and send email for validateResendActivationCode
	 *
	 * @return Response
	 */
	public function validateResendActivationCode()
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'email_address'    => 'required|email' 
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('user/resend-activation-code')
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {
			
			$activation_token = md5(time());
			$token_expiry = date('Y-m-d');
			// create our user data for the forgot password 
			
			$user = DB::table('users')->where('email_address', '=', Input::get('email_address'))->get();
			if(empty($user)){
				return Redirect::to('user/resend-activation-code')->with('error_msg', 'Your provided email address does not found, please try again with correct email address.');
			}else {
				User::where('email_address', '=', Input::get('email_address'))->update(array('activation_token' => $activation_token, 'token_expiry' => $token_expiry, 'is_active' => 'n'));
				
				//Send activation email
				$data['user'] = array('token' => $activation_token, 'first_name' => $user[0]->first_name, 'last_name' => $user[0]->last_name);
				Mail::send('emails.resend_activation_code', $data, function($message) {
					$message->from('tanveer.hussain@nxb.com.pk', 'Tanveer Hussain ~ NXB');
					$message->to(Input::get('email_address'), 'Dear User')->subject('Acount Activation - Laravel members app');
				});
				
				return Redirect::to('user/resend-activation-code')->with('flash_error', 'We have sent you an e-mail message containing a link to active your account. Please check your inbox.');
			}			

		}
	}
	
	/**
	 * Show the form for user to forgot password
	 *
	 * @return Response
	 */
	public function forgot_password()
	{
		// Display user forgot password form
		return View::make('forgot_password'); 
	}
	
	/**
	 * Validate user provided credential and send email for reset password
	 *
	 * @return Response
	 */
	public function validateForgotPassword()
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'email_address'    => 'required|email' 
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('user/forgot-password')
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {
			
			$activation_token = md5(time());
			$token_expiry = date('Y-m-d');
			// create our user data for the forgot password 
			
			$user = DB::table('users')->where('email_address', '=', Input::get('email_address'))->get();
			if(empty($user)){
				return Redirect::to('user/forgot-password')->with('error_msg', 'Your provided email address does not found, please try again with correct email address.');
			}else {
				User::where('email_address', '=', Input::get('email_address'))->update(array('activation_token' => $activation_token, 'token_expiry' => $token_expiry));
				
				//Send activation email
				$data['user'] = array('token' => $activation_token, 'first_name' => $user[0]->first_name, 'last_name' => $user[0]->last_name);
				Mail::send('emails.forgot_password', $data, function($message) {
					$message->from('tanveer.hussain@nxb.com.pk', 'Tanveer Hussain ~ NXB');
					$message->to(Input::get('email_address'), 'Dear User')->subject('Forgot password - Laravel members app');
				});
				
				return Redirect::to('user/forgot-password')->with('flash_error', 'We have sent you an e-mail message containing a link to reset your password. Please check your inbox.');
			}			

		}
	}
	
	
	/**
	 * Validate user provided credential and reset password
	 *
	 * @return Response
	 */
	public function reset_password()
	{	
		$users = DB::table('users')->where('activation_token', '=', Request::segment(3))->get();
		if(empty($users)){
			return Redirect::to('user/thankyou')->with('error_msg', "Your token has been expired, please try again.");
		}else {
			return View::make('reset_password'); 
			
		}
		
	}
	
	
	/**
	 * Validate user provided credential and update reset password
	 *
	 * @return Response
	 */
	public function update_reset_password()
	{	
		$users = DB::table('users')->where('activation_token', '=', Request::segment(3))->get();
		if(empty($users)){
			return Redirect::to('user/thankyou')->with('error_msg', "Your token has been expired, please try again.");
		}else {
			
			$rules = array(
			'password' => 'required|alphaNum|min:3|Confirmed', 
			'password_confirmation' => 'required|alphaNum|min:3' 
			);

			// run the validation rules on the inputs from the form
			$validator = Validator::make(Input::all(), $rules);

			// if the validator fails, redirect back to the form
			if ($validator->fails()) {
			return Redirect::to('user/reset/'.Request::segment(3))
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {
			
			$password = Hash::make(Input::get('password'));
			User::where('activation_token', '=', Request::segment(3))->update(array('activation_token' => '', 'password' => $password, 'is_active' => 'y'));
			// redirect change password page with success message
			return Redirect::to('user/thankyou')->with('flash_msg', 'Your password has been changed.');

		}
			
			
			
		}
		
	}
	
	/**
	 * Show the form for user to edit profile
	 *
	 * @return Response
	 */
	public function edit_profile()
	{
		/*
		  Check user is logged in then display edit profile page
		  If user is not logged in then redirect to user login page
		*/
		
		if (Auth::check()) { 
			$user = Auth::user();
			$id = $user->id;
			// Display user edit profile form
			$user = User::find($id);
			return View::make('edit_profile')
				->with('user', $user);
		}else {
			return Redirect::to('user/login'); 
		}	
		
	}
	
	
	/**
	 * Validate user provided credential and update profile
	 *
	 * @return Response
	 */
	public function update_profile()
	{
		/*
		  Check user is logged in then display edit profile page
		  If user is not logged in then redirect to user login page
		*/
		
		if (Auth::check()) { 
			$user = Auth::user();
		$id = $user->id;
		
		// validate the info, create rules for the inputs
		$rules = array(
			'first_name' => 'required', 
			'last_name' => 'required', 
			'email_address' => 'required|email|unique:users,email_address,'.$id,
			'username' => 'required|unique:users,username,'.$id,
			'address' => 'required',
			'city' => 'required',
			'state' => 'required',
			'zipcode' => 'required',
			'country' => 'required',
			'biography' => 'required'
		);
		
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('user/edit-profile')
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {
			
			$user->first_name = Input::get('first_name');
			$user->last_name = Input::get('last_name');
			$user->email_address = Input::get('email_address');
			$user->username = Input::get('username');
			$user->address = Input::get('address');
			$user->city = Input::get('city');
			$user->state = Input::get('state');
			$user->zipcode = Input::get('zipcode');
			$user->country = Input::get('country');
			$user->biography = Input::get('biography');
			$user->profile_type = Input::get('profile_type');
			$user->save();
			// redirect change password page with success message
			return Redirect::to('user/edit-profile')->with('flash_msg', 'Your profile has been updated.');

		}
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	/**
	 * Validate user provided credential and upload profile picture
	 *
	 * @return Response
	 */
	 
	 public function upload_profile_picture(){
		 /*
		  Check user is logged in then upload profile picture
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			$ImageType = $_FILES['ImageFile']['type'];
			switch(strtolower($ImageType))
			{
				case 'image/png':
					//Create a new image from file 
					$CreatedImage =  imagecreatefrompng($_FILES['ImageFile']['tmp_name']);
					break;
				case 'image/gif':
					$CreatedImage =  imagecreatefromgif($_FILES['ImageFile']['tmp_name']);
					break;			
				case 'image/jpeg':
				case 'image/pjpeg':
					$CreatedImage = imagecreatefromjpeg($_FILES['ImageFile']['tmp_name']);
					break;
				default:
					die('Unsupported File!'); //output error and exit
			}
			
			$extension = Input::file('ImageFile')->getClientOriginalExtension(); 
			$fileName = md5(time()).'.'.$extension;
			$destinationPath = public_path().'\assets\images\users';
			$file = Input::file('ImageFile')->move($destinationPath, $fileName);			
			list($CurWidth,$CurHeight)=getimagesize($file);
			
			$ThumbSquareSize = 137;
			$thumb_DestRandImageName = $destinationPath.'\thumb_'.$fileName;
			$Quality = 90;
			$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
			
			$ThumbSquareSize = 50;
			$thumb_DestRandImageName = $destinationPath.'\avatar_'.$fileName;
			$Quality = 90;
			$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
			
			$destinationPath = public_path().'/assets/images/users/';
			$destinationPath = str_replace('\\', '/', $destinationPath);
			$old_picture = Auth::user()->picture;			
			$old_pictures = array($destinationPath."$old_picture", $destinationPath."thumb_$old_picture");
			File::delete($old_pictures);
			$user = Auth::user();
			$user->picture = $fileName;
			$user->save();
			sleep(1);
			echo '"'.$fileName.'"';
			
						
		}else {
			return Redirect::to('user/login'); 
		}
		
	 }
	 
	 //This function corps image to create exact square images, no matter what its original size!
	  private function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
		{	 
			//Check Image size is not 0
			if($CurWidth <= 0 || $CurHeight <= 0) 
			{
				return false;
			}
			
			//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
			if($CurWidth>$CurHeight)
			{
				$y_offset = 0;
				$x_offset = ($CurWidth - $CurHeight) / 2;
				$square_size 	= $CurWidth - ($x_offset * 2);
			}else{
				$x_offset = 0;
				$y_offset = ($CurHeight - $CurWidth) / 2;
				$square_size = $CurHeight - ($y_offset * 2);
			}
			
			$NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
			if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
			{
				switch(strtolower($ImageType))
				{
					case 'image/png':
						imagepng($NewCanves,$DestFolder);
						break;
					case 'image/gif':
						imagegif($NewCanves,$DestFolder);
						break;			
					case 'image/jpeg':
					case 'image/pjpeg':
						imagejpeg($NewCanves,$DestFolder,$Quality);
						break;
					default:
						return false;
				}
			//Destroy image, frees memory	
			if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
			return true;
		
			}
			  
		}
	
	/**
	 * Show the form for user to change password
	 *
	 * @return Response
	 */
	public function change_password()
	{
		/*
		  Check user is logged in then display change password page
		  If user is not logged in then redirect to user login page
		*/
		
		if (Auth::check()) { 
		  // Display user change password form
		  return View::make('change_password'); 
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	
	/**
	 * Validate user provided credential and update password
	 *
	 * @return Response
	 */
	public function update_password()
	{
		/*
		  Check user is logged in then display change password page
		  If user is not logged in then redirect to user login page
		*/
		
		if (Auth::check()) { 
		// validate the info, create rules for the inputs
		$rules = array(
			'current_password' => 'required|alphaNum|min:3', 
			'password' => 'required|alphaNum|min:3|Confirmed', 
			'password_confirmation' => 'required|alphaNum|min:3' 
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('user/change-password')
				->withErrors($validator) 
				->withInput(Input::except('password')); 
		} else {
			
			$current_password = Input::get('current_password');
			$password = Input::get('password');
			$user = Auth::user();
			
			if (strlen($current_password) > 0 && !Hash::check($current_password, $user->password)) {
        			return Redirect::to('user/change-password')->with('flash_error', 'Your current is mismatched, please try again with correct password.');
		    }
			
			$user->password = Hash::make($password);
			$user->save();
			// redirect change password page with success message
			return Redirect::to('user/change-password')->with('flash_msg', 'Your password has been changed.');

		}
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	
	
	/**
	 * Show the form for user to login
	 *
	 * @return Response
	 */
	public function logout()
	{ 
		// logs out the user and redirect to user login page
		Session::flush(); 
		return Redirect::to('user/login');
	}


}