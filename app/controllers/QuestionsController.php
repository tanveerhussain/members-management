<?php

class QuestionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		/*
		  Check user is logged in then display team page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			
			$questions = array();
			$recipient = array();
			
			if(Auth::user()->id == 1){
				$teams = DB::table('teams')->orderBy('title', 'asc')->get();  			
			}else{
				$teams = DB::table('teams')->where('created_by',Auth::user()->id)->orderBy('title', 'asc')->get();  			
			}
			foreach($teams as $row){
				$recipient[$row->id] = $row->title;
			}
			if(Auth::user()->id <> 1){
				$teams = DB::table('teams')->whereRaw('FIND_IN_SET('.intval(Auth::user()->id).',team_member)')->get();
				foreach($teams as $row){
					$recipient[$row->id] = $row->title;
				}
			}
			
			$questions = DB::table('questions')->where('is_active', '=', 'y')->Where(function($query){ $query->where('created_by', '=', Auth::user()->id); })->orderBy('id', 'desc')->get();
			
			foreach($questions as $row){
				
				$row->answer = str_replace('|||',',', $row->answer);
				
				if($row->recipient != ''){
					$recipient_tmp = explode(',',$row->recipient);
					$teams = DB::table('teams')->whereIn('id',$recipient_tmp)->get();
					$recipient_tmp = '';
					foreach($teams as $t){
						$recipient_tmp .= $t->title.', ';
					}
					$recipient_tmp = rtrim($recipient_tmp, ', ');
				}else{
					$recipient_tmp = 'Ask from public';
				}
				$row->recipient = $recipient_tmp;
				$time = strtotime($row->created_at);
				$row->created_at = $this->humanTiming($time).' ago';
			}
			
			return View::make('my_questions')->with('questions', $questions)->with('recipient', $recipient); 
		}else {
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Save response for question.
	 *
	 * @return Response
	 */
	public function save_response()
	{
		if (Auth::check()) { 
			$rules = array(
				'response' => 'required_if:question_type,open,check-in',
				'location' => 'required_if:question_type,check-in',
				'picture' => 'sometimes|image',
				'single_response' => 'required_if:question_type,single',
				'multiple_response' => 'required_if:question_type,multiple'
		    );
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) { 
				return Redirect::to('/questions/'.Input::get('question_id'))->withErrors($validator)->withInput(); 
			}else{
				$question_type = Input::get('question_type');
				$question_id = Input::get('question_id');
				
				$response = '';
				$location = '';
				$fileName = '';
				
				if($question_type == 'open' or $question_type == 'check-in'){
					$response = Input::get('response');
					if($question_type == 'check-in'){
						$location = Input::get('location');
						if (Input::hasFile('picture')){
							$ImageType = $_FILES['picture']['type'];
							switch(strtolower($ImageType))
							{
								case 'image/png':
									//Create a new image from file 
									$CreatedImage =  imagecreatefrompng($_FILES['picture']['tmp_name']);
									break;
								case 'image/gif':
									$CreatedImage =  imagecreatefromgif($_FILES['picture']['tmp_name']);
									break;			
								case 'image/jpeg':
								case 'image/pjpeg':
									$CreatedImage = imagecreatefromjpeg($_FILES['picture']['tmp_name']);
									break;
								default:
									die('Unsupported File!'); //output error and exit
							}
					
							$destinationPath = public_path().'\assets\images\questions';
							$extension = Input::file('picture')->getClientOriginalExtension(); 
							$fileName = md5(time()).'.'.$extension;
							$file = Input::file('picture')->move($destinationPath, $fileName);			
							list($CurWidth,$CurHeight)=getimagesize($file);
					
						
							$ThumbSquareSize = 137;
							$thumb_DestRandImageName = $destinationPath.'\thumb_'.$fileName;
							$Quality = 90;
							$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
							
					
					
					
						}
					}
				}				
				
				if($question_type == 'single'){
					$response = Input::get('single_response');
				}
				
				if($question_type == 'multiple'){
					$response = implode('|||',Input::get('multiple_response'));
				}
				
				$res = new Comment;
				$res->response = $response;
				$res->location = $location;
				$res->picture = $fileName;
				$res->question_id = $question_id;
				$res->response_by = Auth::user()->id;
				$res->is_active = 'y';
				$res->save();
				
				return Redirect::to('/questions/'.Input::get('question_id'))->with('flash_msg', 'Your response has been successfully saved.');

				
			}
		}else{
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{ 	
		if (Auth::check()) { 
			$rules = array(
				'question'    => 'required',
				'recipient' => 'required_if:recipient_type,team'
		    );
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) { 
				return Redirect::to('/questions')->withErrors($validator)->withInput(); 
			}else{				
			
				$question_type = Input::get('question_type');
				
				$answer = '';
				if($question_type == 'single' or $question_type == 'multiple'){
					$answer = implode('|||',Input::get('multiple_answers'));
				}
				
				$recipient = '';	
				$recipient_type = Input::get('recipient_type');
				if($recipient_type == 'team'){
					$recipient = implode(',',Input::get('recipient'));	
				}
				
				$question = new Question;
				$question->question = Input::get('question');
				$question->question_type = $question_type;
				$question->answer = $answer;
				$question->recipient_type = $recipient_type;
				$question->recipient = $recipient;
				$question->status = Input::get('status');
				$question->created_by = Auth::user()->id;
				$question->is_active = 'y';
				$question->save();
				
				if(Input::get('status') == 'publish'){
					require_once('Pusher.php');
					$app_key = '7f9bf1784d5828d957b6';
					$app_secret = '04428c0b40a0c42481d5';
					$app_id = '81437';
					
					$pusher = new Pusher($app_key, $app_secret, $app_id);
					$data = array('message' => Input::get('question'));
					
					$users_ids = array();
					if($recipient_type == 'public'){
						$users_ids = DB::table('users')->lists('id');
					}else{
						if($recipient!=""){
							$recipient_teams = explode(',', $recipient);
							$users_list = DB::table('teams')->whereIn('id' , $recipient_teams)->lists('id');
							foreach($users_list as $u){
								$u = explode(',',$u);
								foreach($u as $tu){
									$users_ids[] = $tu;
								}
							}
								
						}
					}
					
					if(count($users_ids) > 0){
						$users_ids = array_unique($users_ids);
						foreach($users_ids as $u){
							$pusher->trigger('private-'.$u, 'notification', $data);
						}
					}
					
				}

				
				if($recipient_type == 'team' and Input::get('status') == 'publish'){
					if($recipient!=""){
						$recipient = explode(',', $recipient);
						$teams = DB::table('teams')->whereIn('id',$recipient)->get();
						$user_ids = array();
						foreach($teams as $t){
							$team_member = explode(',',$t->team_member);
							foreach($team_member as $tm){
								$user_ids[] = $tm;
							}
						}
						
						if(!empty($user_ids)){
							$user_ids = array_unique($user_ids);
							$users = DB::table('users')->whereIn('id',$user_ids)->get();
							$data['user'] = array('question_title' => Input::get('question'), 'first_name' => Auth::user()->first_name, 'last_name' => Auth::user()->last_name);
							foreach($users as $mailuser){
								Mail::queue('emails.question_invitation', $data, function($message) use ($mailuser) {
									$message->from(Auth::user()->email_address, Auth::user()->first_name.' '.Auth::user()->last_name);
									$message->to($mailuser->email_address, 'Dear User')->subject('Question asked from you - Laravel members app');
								});
							}
						}
						
					}
				}
				
				return Redirect::to('/questions')->with('flash_msg', 'You have successfully added new question.');
			}
		}else{
			return Redirect::to('user/login'); 
		}
	}
	
	public function auth(){
		
		require_once('Pusher.php');
		if (Auth::user()->id == substr(Input::get('channel_name'), 8)) {
			$app_key = '7f9bf1784d5828d957b6';
			$app_secret = '04428c0b40a0c42481d5';
			$app_id = '81437';			
			$pusher = new Pusher($app_key, $app_secret, $app_id);
			echo $pusher->socket_auth(Input::get('channel_name'), Input::get('socket_id'));
		}else{
			header('', true, 403);
			echo "Forbidden";
		}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		/*
		  Check user is logged in then display detail page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) {
			$question = DB::table('questions')->where('id', '=', $id)->get();
			$myquestion = array();
			foreach($question as $q){
				foreach($q as $index=>$r){
					if($index == 'recipient'){
						if($r != ''){
							$r = explode(',', $r);
						}
					}
					
					if($index == 'answer'){
						$myquestion['multiple_answers'] = array('');
						if($r != ''){
							$temparray = explode('|||', $r);
							if(count($temparray) > 1){
								$myquestion['multiple_answers'] = $temparray;
								$r = '';
							}
						}
					}
					
					$myquestion[$index] = $r;
				}
			}
			if(empty($question)){			
				return Redirect::to('/'); 
			}else{
				$question = Question::find($id);
				$question->num_views = $question->num_views + 1;
				$question->save();
				$comment = DB::table('comments')->where('question_id', '=', $id)->where('response_by', '=', Auth::user()->id)->get();
				
				$responses = array();
				if($myquestion['created_by'] == Auth::user()->id){
					if($myquestion['question_type'] == 'single'){
						foreach($myquestion['multiple_answers'] as $aindex=>$multiple_answers){
							$total = DB::table('comments')->where('question_id', '=', $id)->where('response', '=', $multiple_answers)->count();
							$responses[$aindex]['answer'] = $multiple_answers;
							$responses[$aindex]['total'] = $total;
						}
					}
					
					if($myquestion['question_type'] == 'multiple'){
						$dbcomments = DB::table('comments')->where('question_id', '=', $id)->get();
						
						foreach($dbcomments as $com){
							$dbresponse = explode("|||", $com->response);
							foreach($myquestion['multiple_answers'] as $aindex=>$multiple_answers){
								$total = 0;
								if(isset($responses[$aindex]['total'])){
									$total = $responses[$aindex]['total'];
								}
								if(in_array($multiple_answers, $dbresponse)){
									$total++;
								}
								$responses[$aindex]['answer'] = $multiple_answers;
								$responses[$aindex]['total'] = $total;
							}
							
						}
						
					}
					
					if($myquestion['question_type'] == 'check-in' or $myquestion['question_type'] == 'open'){
						$responses = DB::table('comments')->join('users', 'users.id', '=', 'comments.response_by')->select('comments.response', 'comments.location', 'comments.picture', 'users.first_name', 'users.last_name')->where('question_id', '=', $id)->get();
							
					}
					
				}
				
				return View::make('question_detail')->with('question', $myquestion)->with('comment', $comment)->with('responses', $responses);
			}
			
		}else{
			return Redirect::to('user/login'); 
		}
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		/*
		  Check user is logged in then display edit page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) {
			
			// get the question from database using id
			$question = DB::table('questions')->where('id', '=', $id)->Where(function($query){ $query->where('created_by', '=', Auth::user()->id); })->get();
			
			$myquestion = array();
			foreach($question as $q){
				foreach($q as $index=>$r){
					if($index == 'recipient'){
						if($r != ''){
							$r = explode(',', $r);
						}
					}
					
					if($index == 'answer'){
						$myquestion['multiple_answers'] = array('');
						if($r != ''){
							$temparray = explode('|||', $r);
							if(count($temparray) > 1){
								$myquestion['multiple_answers'] = $temparray;
								$r = '';
							}
						}
					}
					
					$myquestion[$index] = $r;
				}
			}
			
			if(empty($question)){			
				return Redirect::to('questions'); 	
			}else{
				
				$recipient = array();
				
				if(Auth::user()->id == 1){
					$teams = DB::table('teams')->orderBy('title', 'asc')->get();  			
				}else{
					$teams = DB::table('teams')->where('created_by',Auth::user()->id)->orderBy('title', 'asc')->get();  			
				}				
				
				foreach($teams as $row){
					$recipient[$row->id] = $row->title;
				}
				
				return View::make('edit_question')->with('question', $myquestion)->with('recipient', $recipient);
				
			}
			
		}else {
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if (Auth::check()) { 
			$rules = array(
				'question'    => 'required',
				'answer' => 'required_if:question_type,open,check-in',
				'recipient' => 'required_if:recipient_type,team'
		    );
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::to('/questions/'.$id.'/edit')->withErrors($validator)->withInput(); 
			}else{
				
				$question_type = Input::get('question_type');
				
				$answer = '';
				if($question_type == 'open' or $question_type == 'check-in'){
					$answer = Input::get('answer');
				}
				
				if($question_type == 'single' or $question_type == 'multiple'){
					$answer = implode('|||',Input::get('multiple_answers'));
				}
				
				$recipient = '';	
				$recipient_type = Input::get('recipient_type');
				if($recipient_type == 'team'){
					$recipient = implode(',',Input::get('recipient'));	
				}
				
				$question = Question::find($id);
				$question->question = Input::get('question');
				$question->question_type = $question_type;
				$question->answer = $answer;
				$question->recipient_type = $recipient_type;
				$question->recipient = $recipient;
				$question->status = Input::get('status');
				$question->created_by = Auth::user()->id;
				$question->is_active = 'y';
				$question->save();
				
				return Redirect::to('/questions')->with('flash_msg', 'You have successfully updated question.');
			}
		}else{
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		/*
		  Check user is logged in then remove question
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) {
			if($id!=""){
				$question = DB::table('questions')->where('id', '=', $id)->Where(function($query){ $query->where('created_by', '=', Auth::user()->id); })->get();
				if(empty($question)){			
					return Redirect::to('questions'); 	
				}else{
					$question = Question::find($id);
					$question->is_active = 'n';
					$question->save();
					return Redirect::to('/questions')->with('flash_msg', 'You have successfully remove question.');
				}
			}
			
			return Redirect::to('questions'); 	
				
		}else{
			return Redirect::to('user/login'); 
		}
	}
	
	//This function corps image to create exact square images, no matter what its original size!
	  private function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
		{	 
			//Check Image size is not 0
			if($CurWidth <= 0 || $CurHeight <= 0) 
			{
				return false;
			}
			
			//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
			if($CurWidth>$CurHeight)
			{
				$y_offset = 0;
				$x_offset = ($CurWidth - $CurHeight) / 2;
				$square_size 	= $CurWidth - ($x_offset * 2);
			}else{
				$x_offset = 0;
				$y_offset = ($CurHeight - $CurWidth) / 2;
				$square_size = $CurHeight - ($y_offset * 2);
			}
			
			$NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
			if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
			{
				switch(strtolower($ImageType))
				{
					case 'image/png':
						imagepng($NewCanves,$DestFolder);
						break;
					case 'image/gif':
						imagegif($NewCanves,$DestFolder);
						break;			
					case 'image/jpeg':
					case 'image/pjpeg':
						imagejpeg($NewCanves,$DestFolder,$Quality);
						break;
					default:
						return false;
				}
			//Destroy image, frees memory	
			if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
			return true;
		
			}
			  
		}

	private function humanTiming ($time)
	{
		
			$time = time() - $time; // to get the time since that moment
			$tokens = array (
				31536000 => 'year',
				2592000 => 'month',
				604800 => 'week',
				86400 => 'day',
				3600 => 'hour',
				60 => 'minute',
				1 => 'second'
			);
		
			foreach ($tokens as $unit => $text) {
				if ($time < $unit) continue;
				$numberOfUnits = floor($time / $unit);
				return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
			}
		
     }

}
