<?php

class HomeController extends \BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@index');
	|
	*/

	public function index()
	{
		/*
		  Check user is logged in then display dashboard page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			$users = array();
			$invited_questions = array();
			
			// create by logged in user
			$question = DB::table('questions')->where('status', '=', 'publish')->Where(function($query){ $query->where('created_by', '=', Auth::user()->id); 
				$query->where('is_active', '=', 'y'); })->get();
			if(count($question) > 0){
				foreach($question as $q){
					$invited_questions[$q->id] = $q;
				}
			}
			
			// public questions
			$question = DB::table('questions')->where('status', '=', 'publish')->Where(function($query){ $query->where('recipient', '=', ''); $query->where('is_active', '=', 'y'); })->get();
			if(count($question) > 0){
				foreach($question as $q){
					$invited_questions[$q->id] = $q;
				}
			}

			// asked question from logged in user
			$teams = DB::table('teams')->whereRaw('FIND_IN_SET('.intval(Auth::user()->id).',team_member)')->get();
			if(count($teams) > 0){
				foreach($teams as $row){
					$question = DB::table('questions')->where('status', '=', 'publish')->whereRaw('FIND_IN_SET('.intval($row->id).',recipient)')->get();
					if(count($question) > 0){
						foreach($question as $q){
							$invited_questions[$q->id] = $q;
						}
					}
				}
			}
			
			foreach($invited_questions as $q){
				$num_responses = Comment::where('question_id', '=', $q->id)->count();
				$user = User::find($q->created_by);
				$q->created_by = $user->first_name.' '.$user->last_name;
				$q->created_by_picture = $user->picture;
				$q->num_responses = $num_responses;
				$q->created_date = $q->created_at;
				$time = strtotime($q->created_at);
				$q->created_at = $this->humanTiming($time).' ago';
			}
			
			// Object Oriented
			uasort($invited_questions, array($this, 'cmp')); 
				
			return View::make('dashboard')->with('users', $users)->with('keyword', Input::old('keyword'))->with('invited_questions', $invited_questions); 
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	public function filter_questions()
	{
		sleep(1);	
		
		if (Auth::check()) { 
			$filter = Input::get('filter');
			$invited_questions = array();
			
			// create by logged in user
			if($filter == 'all' or $filter == 'created_by'){
				$question = DB::table('questions')->where('status', '=', 'publish')->Where(function($query){ $query->where('created_by', '=', Auth::user()->id); 
					$query->where('is_active', '=', 'y'); })->get();
				if(count($question) > 0){
					foreach($question as $q){
						$invited_questions[$q->id] = $q;
					}
				}
			}
			
			// public questions
			if($filter == 'all'){
				$question = DB::table('questions')->where('status', '=', 'publish')->Where(function($query){ $query->where('recipient', '=', ''); $query->where('is_active', '=', 'y'); })->get();
				if(count($question) > 0){
					foreach($question as $q){
						$invited_questions[$q->id] = $q;
					}
				}
			}

			// asked question from logged in user
			if($filter == 'all' or $filter == 'asked'){
			$teams = DB::table('teams')->whereRaw('FIND_IN_SET('.intval(Auth::user()->id).',team_member)')->get();
				if(count($teams) > 0){
					foreach($teams as $row){
						$question = DB::table('questions')->where('status', '=', 'publish')->whereRaw('FIND_IN_SET('.intval($row->id).',recipient)')->get();
						if(count($question) > 0){
							foreach($question as $q){
								$invited_questions[$q->id] = $q;
							}
						}
					}
				}
			}
			
			$final_invited_questions = array();
			foreach($invited_questions as $q){
				$num_responses = Comment::where('question_id', '=', $q->id)->count();
				$user = User::find($q->created_by);
				$q->created_by = $user->first_name.' '.$user->last_name;
				$q->created_by_picture = $user->picture;
				$q->num_responses = $num_responses;
				$q->created_date = $q->created_at;
				$time = strtotime($q->created_at);
				$q->created_at = $this->humanTiming($time).' ago';
			}
			uasort($invited_questions, array($this, 'cmp'));
			return View::make('dashboard_mini')->with('invited_questions', $invited_questions); 
		}else{
			echo '<tr><td align="center" valign="top" colspan="5"><p class="error">No Record Found</p></td></tr>';
		}
	}
	
	public function search_members()
	{
		/*
		  Check user is logged in then display dashboard page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
		  
		    // validate the info, create rules for the inputs
			$rules = array(
				'keyword'    => 'required' 
		    );
			// run the validation rules on the inputs from the form
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::to('/')->withErrors($validator); 
			}else{
				$users = DB::table('users')->where('profile_type', '=', 'public')
						->Where(function($query)
						{
							$query->where('first_name', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('last_name', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('email_address', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('username', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('address', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('city', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('state', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('zipcode', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('country', 'LIKE', '%'.Input::get('keyword').'%')
								  ->orwhere('biography', 'LIKE', '%'.Input::get('keyword').'%')
								  ;
						})->get();
				
				if(empty($users)){
					return Redirect::to('/')->with('flash_error', 'No result found, please try another keyword')->withInput(Input::all());
				}
				return View::make('dashboard')->with('users', $users)->with('keyword', Input::get('keyword')); 
			}
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	public function my_friends()
	{
		/*
		  Check user is logged in then display dashboard page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
		  
		  		$friend_ids = array();
							
				$results = DB::table('friends')->where('user_id', Auth::user()->id)->get();
				if(!empty($results)){
					foreach($results as $row){
						$friend_ids[] = $row->invited_by;
					}
				}
				
				$results = DB::table('friends')->where('invited_by', Auth::user()->id)->get();
				
				if(!empty($results)){
					foreach($results as $row){
						$friend_ids[] = $row->user_id;
					}
				}
				
				$friend_ids = array_unique($friend_ids);
				if(count($friend_ids) > 0){
				 $users = DB::table('users')->whereIn('id',$friend_ids)->get();
				}else{
					$users = array();
				}
				return View::make('my_friends')->with('users', $users); 
			
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	
	public function invite_friends()
	{
		/*
		  Check user is logged in then display invite friends page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			return View::make('invite_friends'); 
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	public function send_invitation()
	{
		/*
		  Check user is logged in then send invitation
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
		  
		    // validate the info, create rules for the inputs
			$rules = array(
				'email_address'    => 'required',
				'subject'    => 'required',
				'message'    => 'required'
		    );
			// run the validation rules on the inputs from the form
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::to('/invite-friends')->withErrors($validator)->withInput();  
			}else{
				
				$to_emails = Input::get('email_address');
				
				if (Input::hasFile('import_emails_via_csv')){
					
					$mime_types = array('text/plain','text/comma-separated-values','text/csv','application/csv','application/excel','application/vnd.ms-excel','application/vnd.msexcel');
					$mime = Input::file('import_emails_via_csv')->getMimeType();
					if(in_array($mime, $mime_types)){
						
					$destinationPath = public_path().'\assets\uploads\csv';
					$extension = Input::file('import_emails_via_csv')->getClientOriginalExtension(); 
					$fileName = md5(time()).'.'.$extension;
					$csvFile = Input::file('import_emails_via_csv')->move($destinationPath, $fileName);			
					$csv = $this->readCSV($csvFile);
					foreach ($csv as $listings) {
					 if(is_array($listings)){	
					 foreach ($listings as $csv_email) {
						$is_found = DB::table('invitations')->where('email_address', '=', $csv_email)->get();
						if(empty($is_found)){
							$invitation = new Invitation;
							$invitation->email_address = $csv_email;
							$invitation->invited_by = Auth::user()->id;
							$invitation->is_register = 'n';
							$invitation->save();
							$to_emails .= ','.$csv_email;
						}
					 }
					 }
					}
					
					$destinationPath = str_replace('\\', '/', $destinationPath);
					File::delete($destinationPath."/$fileName");
					
					}else {
						return Redirect::to('/invite-friends')->withInput()->with('flash_error', 'Please upload a valid file with CSV extension.');  
					}
					
					
					
				}
				
				
				$is_found = DB::table('invitations')->where('email_address', '=', Input::get('email_address'))->get();
				if(empty($is_found)){
					$invitation = new Invitation;
					$invitation->email_address = Input::get('email_address');
					$invitation->invited_by = Auth::user()->id;
					$invitation->is_register = 'n';
					$invitation->save();
				}
				
				
				
				
				//echo $to_emails; die;
				$to_emails = explode(',', $to_emails);
				$to_emails = array_unique($to_emails);
				//Send invitation email
				$data['user'] = array('message' => Input::get('message'), 'first_name' => Auth::user()->first_name, 'last_name' => Auth::user()->last_name);
				foreach ($to_emails as $mailuser) {
					Mail::queue('emails.invitation', $data, function($message) use ($mailuser) {
						$message->from(Auth::user()->email_address, Auth::user()->first_name.' '.Auth::user()->last_name);
						$message->to($mailuser, 'Dear User')->subject(Input::get('subject').' - Laravel members app');
					});
				}
				
				return Redirect::to('/invite-friends')->with('flash_msg', 'Thank you for recommendation! Your invitation has been sent successfully.');
			}
		}else {
			return Redirect::to('user/login'); 
		}
	}
	
	private function readCSV($csvFile) {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);
        return $line_of_text;
    }
	
	private function humanTiming ($time)
	{
		
			$time = time() - $time; // to get the time since that moment
			$tokens = array (
				31536000 => 'year',
				2592000 => 'month',
				604800 => 'week',
				86400 => 'day',
				3600 => 'hour',
				60 => 'minute',
				1 => 'second'
			);
		
			foreach ($tokens as $unit => $text) {
				if ($time < $unit) continue;
				$numberOfUnits = floor($time / $unit);
				return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
			}
		
     }
	 
	private function cmp($a, $b) {
			if ($a->created_date == $b->created_date) {
				return 0;
			}
			return ($a->created_date < $b->created_date) ? 1 : -1;
	} 

}
