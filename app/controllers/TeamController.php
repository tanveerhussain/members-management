<?php

class TeamController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		/*
		  Check user is logged in then display team page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			
				$friend_ids = array();
							
				$results = DB::table('friends')->where('user_id', Auth::user()->id)->get();
				if(!empty($results)){
					foreach($results as $row){
						$friend_ids[] = $row->invited_by;
					}
				}
				
				$results = DB::table('friends')->where('invited_by', Auth::user()->id)->get();
				
				if(!empty($results)){
					foreach($results as $row){
						$friend_ids[] = $row->user_id;
					}
				}
				
				$friend_ids = array_unique($friend_ids);
				if(count($friend_ids) > 0){
					$users = DB::table('users')->whereIn('id',$friend_ids)->get();
				}else{
					$users = array();
				}
				
				$myfriends = array();
				foreach($users as $u){
					$myfriends[$u->id] = $u->first_name.' '.$u->last_name;
				}
			
			$teams = DB::table('teams')->where('created_by',Auth::user()->id)->get();  
			foreach($teams as $row){
				
				$team_member = explode(',',$row->team_member);
				$users = DB::table('users')->whereIn('id',$team_member)->get();
				$team_member = '';
				foreach($users as $u){
					$picture = URL::asset('assets/images/avatar_profile_picture.png');
					if($u->picture){
						$picture = URL::asset('assets/images/users/avatar_'.$u->picture);
					}
					$team_member .= '<img src="'.$picture.'" alt="'.$u->first_name.' '.$u->last_name.'" title="'.$u->first_name.' '.$u->last_name.'" >';
				}
				
				$users = DB::table('teaminvitations')->where('team_id',$row->id)->get();
				if(!empty($users)){
					foreach($users as $u){
						$picture = URL::asset('assets/images/avatar_profile_picture.png');
						$team_member .= '<img src="'.$picture.'" alt="'.$u->first_name.' '.$u->last_name.'" title="'.$u->first_name.' '.$u->last_name.'" >';
					}
				}
				
				$row->team_member = $team_member;
			}
			return View::make('my_teams')->with('teams', $teams)->with('myfriends', $myfriends); 
		}else {
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Show the form for creating a my teams page.
	 *
	 * @return Response
	 */
	public function my_teams()
	{
		/*
		  Check user is logged in then display team page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			
			$teams = DB::table('teams')->whereRaw('FIND_IN_SET('.intval(Auth::user()->id).',team_member)')->get();
			foreach($teams as $row){
				
				$team_member = explode(',',$row->team_member);
				$users = DB::table('users')->whereIn('id',$team_member)->get();
				$team_member = '';
				foreach($users as $u){
					$picture = URL::asset('assets/images/avatar_profile_picture.png');
					if($u->picture){
						$picture = URL::asset('assets/images/users/avatar_'.$u->picture);
					}
					$team_member .= '<img src="'.$picture.'" alt="'.$u->first_name.' '.$u->last_name.'" title="'.$u->first_name.' '.$u->last_name.'" >';
				}
				
				$row->team_member = $team_member;
			}
			return View::make('teams')->with('teams', $teams); 
		}else {
			return Redirect::to('user/login'); 
		}
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function update_team(){
		/*
		  Check user is logged in then display team page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) {
			$id = Input::get('team_id');
			$team = Team::find($id);
			
			$team_member = $team->team_member;
			
			$team_member_expo = explode(',', $team_member);
			if(count($team_member_expo) > 1){
				$team_member = str_replace(Auth::user()->id.',', '', $team_member);
				$team_member = str_replace(','.Auth::user()->id, '', $team_member);
			}else {
				$team_member = '';
			}
			
			$team->team_member = $team_member;
			$team->save();			
			return Redirect::to('/my-teams')->with('flash_msg', 'You have successfully leave that team.');
		}else{
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::check()) { 
			$rules = array(
				'title'    => 'required',
				'description'    => 'required',
				'team_avatar'    => 'sometimes|image',
				'first_name01'    => 'required_if:team_member,',
				'last_name01'    => 'required_if:team_member,',
				'email_address01'    => 'required_if:team_member,|email'
		    );
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::to('/team')->withErrors($validator)->withInput(); 
			}else{
				$fileName = '';
				if (Input::hasFile('team_avatar')){
					
					$ImageType = $_FILES['team_avatar']['type'];
					switch(strtolower($ImageType))
					{
						case 'image/png':
							//Create a new image from file 
							$CreatedImage =  imagecreatefrompng($_FILES['team_avatar']['tmp_name']);
							break;
						case 'image/gif':
							$CreatedImage =  imagecreatefromgif($_FILES['team_avatar']['tmp_name']);
							break;			
						case 'image/jpeg':
						case 'image/pjpeg':
							$CreatedImage = imagecreatefromjpeg($_FILES['team_avatar']['tmp_name']);
							break;
						default:
							die('Unsupported File!'); //output error and exit
					}
					
					$destinationPath = public_path().'\assets\images\teams';
					$extension = Input::file('team_avatar')->getClientOriginalExtension(); 
					$fileName = md5(time()).'.'.$extension;
					$file = Input::file('team_avatar')->move($destinationPath, $fileName);			
					list($CurWidth,$CurHeight)=getimagesize($file);
					
				
					$ThumbSquareSize = 137;
					$thumb_DestRandImageName = $destinationPath.'\thumb_'.$fileName;
					$Quality = 90;
					$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
				
					
				}
				
				$team_member = '';
				if(count(Input::get('team_member')) > 0){
					$team_member = implode(',',Input::get('team_member'));
				}
				
				$team = new Team;
				$team->title = Input::get('title');
				$team->description = Input::get('description');
				$team->team_member = $team_member;
				$team->team_avatar = $fileName;
				$team->created_by = Auth::user()->id;
				$team->is_active = 'y';
				$team->save();
				$lastInsertedId = $team->id;
				
				$to_emails = '';
				if(Input::get('email_address01')){
					
					$tinvitation = new Teaminvitation;
					$tinvitation->first_name = Input::get('first_name01');
					$tinvitation->last_name = Input::get('last_name01');
					$tinvitation->email_address = Input::get('email_address01');
					$tinvitation->team_id = $lastInsertedId;
					$tinvitation->save();
					
					$to_emails = Input::get('email_address01');
				}
				
				if (Input::has('email')){	
					$first_name = Input::get('first_name');
					$last_name = Input::get('last_name');
					foreach(Input::get('email') as $eindex=>$evalue){
						
						$tinvitation = new Teaminvitation;
						$tinvitation->first_name = $first_name[$eindex];
						$tinvitation->last_name = $last_name[$eindex];
						$tinvitation->email_address = $evalue;
						$tinvitation->team_id = $lastInsertedId;
						$tinvitation->save();
						
						$to_emails .= ','.$evalue;
						
					}
				}
				
				
				$to_emails = explode(',', $to_emails);
				$to_emails = array_unique($to_emails);
				//Send invitation email
				$data['user'] = array('message' => '', 'first_name' => Auth::user()->first_name, 'last_name' => Auth::user()->last_name);
				foreach ($to_emails as $mailuser) {
					Mail::queue('emails.invitation', $data, function($message) use ($mailuser) {
						$message->from(Auth::user()->email_address, Auth::user()->first_name.' '.Auth::user()->last_name);
						$message->to($mailuser, 'Dear User')->subject('Invitation request - Laravel members app');
					});
				}
				
				
				return Redirect::to('/team')->with('flash_msg', 'You have successfully added new team.');
				
			}
		}else{
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		/*
		  Check user is logged in then display team edit page
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			// get the team from database using id
			$team = DB::table('teams')->where('id', '=', $id)->Where(function($query){ $query->where('created_by', '=', Auth::user()->id); })->get();
			
			$myteam = array();
			foreach($team as $t){
				foreach($t as $index=>$r){
					if($index == 'team_member'){
						if($r != ''){
							$r = explode(',', $r);
						}
					}
					$myteam[$index] = $r;
				}
			}
			
			$teaminvitations = DB::table('teaminvitations')->where('team_id', '=', $id)->get();
			if(count($teaminvitations) > 0){
				
				foreach($teaminvitations as $tindex=>$mem){
					if($tindex == 0){
						$myteam['first_name01'] = $mem->first_name;
						$myteam['last_name01'] = $mem->last_name;
						$myteam['email_address01'] = $mem->email_address;
					}else{
						$myteam['new_members'][$tindex]['first_name'] = $mem->first_name;
						$myteam['new_members'][$tindex]['last_name'] = $mem->last_name;
						$myteam['new_members'][$tindex]['email_address'] = $mem->email_address;
					}
				}
			}else{
				$myteam['first_name01'] = '';
				$myteam['last_name01'] = '';
				$myteam['email_address01'] = '';
				$myteam['new_members'] = array();
			}
			
			if(empty($team)){			
				return Redirect::to('team'); 	
			}else{
				
				$friend_ids = array();
							
				$results = DB::table('friends')->where('user_id', Auth::user()->id)->get();
				if(!empty($results)){
					foreach($results as $row){
						$friend_ids[] = $row->invited_by;
					}
				}
				
				$results = DB::table('friends')->where('invited_by', Auth::user()->id)->get();
				
				if(!empty($results)){
					foreach($results as $row){
						$friend_ids[] = $row->user_id;
					}
				}
				
				if(!empty($myteam['team_member'])){
					foreach($myteam['team_member'] as $row){
							$friend_ids[] = $row;
					}
				}
				
				$friend_ids = array_unique($friend_ids);
				if(!empty($friend_ids)){
					$users = DB::table('users')->whereIn('id',$friend_ids)->get();
				}else{
					$users = array();
				}
				
				$myfriends = array();
				foreach($users as $u){
					$myfriends[$u->id] = $u->first_name.' '.$u->last_name;
				}
				
				return View::make('edit_team')->with('team', $myteam)->with('myfriends', $myfriends);
			}
		}else{
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		/*
		  Check user is logged in then update team
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
			
			$rules = array(
				'title'    => 'required',
				'description'    => 'required',
				'team_avatar'    => 'sometimes|image',
				'first_name01'    => 'required_if:team_member,',
				'last_name01'    => 'required_if:team_member,',
				'email_address01'    => 'required_if:team_member,|email'
		    );
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return Redirect::to('/team/'.$id.'/edit')->withErrors($validator)->withInput(); 
			}else{
				$fileName = Input::get('old_avatar');
				if (Input::hasFile('team_avatar')){
					
					$ImageType = $_FILES['team_avatar']['type'];
					switch(strtolower($ImageType))
					{
						case 'image/png':
							//Create a new image from file 
							$CreatedImage =  imagecreatefrompng($_FILES['team_avatar']['tmp_name']);
							break;
						case 'image/gif':
							$CreatedImage =  imagecreatefromgif($_FILES['team_avatar']['tmp_name']);
							break;			
						case 'image/jpeg':
						case 'image/pjpeg':
							$CreatedImage = imagecreatefromjpeg($_FILES['team_avatar']['tmp_name']);
							break;
						default:
							die('Unsupported File!'); //output error and exit
					}
					
					$destinationPath = public_path().'\assets\images\teams';
					$extension = Input::file('team_avatar')->getClientOriginalExtension(); 
					$fileName = md5(time()).'.'.$extension;
					$file = Input::file('team_avatar')->move($destinationPath, $fileName);			
					list($CurWidth,$CurHeight)=getimagesize($file);
					
				
					$ThumbSquareSize = 137;
					$thumb_DestRandImageName = $destinationPath.'\thumb_'.$fileName;
					$Quality = 90;
					$this->cropImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName,$CreatedImage, $Quality, $ImageType);
					
					$destinationPath = str_replace('\\', '/', $destinationPath);
					
					$old_avatar = Input::get('old_avatar');
					File::delete($destinationPath."/$old_avatar");
					File::delete($destinationPath."/thumb_$old_avatar");
					
					
				}		
				
				$team_member = '';
				if(count(Input::get('team_member')) > 0){
					$team_member = implode(',',Input::get('team_member'));
				}
				
				$team = Team::find($id);
				$team->title = Input::get('title');
				$team->description = Input::get('description');
				$team->team_member = $team_member;
				$team->team_avatar = $fileName;
				$team->save();
				
				Teaminvitation::where('team_id', '=', $id)->where('email_address', '=', Input::get('email_address01'))->update(array('first_name' => Input::get('first_name01'), 'last_name' => Input::get('last_name01')));
				
				$email_address = Input::get('email_address');
				if(isset($email_address) && count($email_address) > 0){
					$first_name = Input::get('first_name');
					$last_name = Input::get('last_name');
					foreach($email_address as $tindex=>$evalue){
						Teaminvitation::where('team_id', '=', $id)->where('email_address', '=', $evalue)->update(array('first_name' => $first_name[$tindex], 'last_name' => $last_name[$tindex]));
					}
				}
				
					
				return Redirect::to('/team')->with('flash_msg', 'You have successfully updated team.');
				
			}			
		}else{
			return Redirect::to('user/login'); 
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		/*
		  Check user is logged in then delete team
		  If user is not logged in then redirect to user login page
		*/
		if (Auth::check()) { 
		
			$fileName = Input::get('old_avatar');
			$destinationPath = public_path().'/assets/images/teams';
			File::delete($destinationPath."/thumb_$fileName");
			File::delete($destinationPath."/$fileName");
			
			$team = Team::find($id);
			$team->delete();
			
			Teaminvitation::where('team_id', '=', $id)->delete();
			
			return Redirect::to('/team')->with('flash_msg', 'Team has been successfully deleted.');
		
		}else{
			return Redirect::to('user/login'); 
		}
		
	}
	
	//This function corps image to create exact square images, no matter what its original size!
	  private function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
		{	 
			//Check Image size is not 0
			if($CurWidth <= 0 || $CurHeight <= 0) 
			{
				return false;
			}
			
			//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
			if($CurWidth>$CurHeight)
			{
				$y_offset = 0;
				$x_offset = ($CurWidth - $CurHeight) / 2;
				$square_size 	= $CurWidth - ($x_offset * 2);
			}else{
				$x_offset = 0;
				$y_offset = ($CurHeight - $CurWidth) / 2;
				$square_size = $CurHeight - ($y_offset * 2);
			}
			
			$NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
			if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
			{
				switch(strtolower($ImageType))
				{
					case 'image/png':
						imagepng($NewCanves,$DestFolder);
						break;
					case 'image/gif':
						imagegif($NewCanves,$DestFolder);
						break;			
					case 'image/jpeg':
					case 'image/pjpeg':
						imagejpeg($NewCanves,$DestFolder,$Quality);
						break;
					default:
						return false;
				}
			//Destroy image, frees memory	
			if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
			return true;
		
			}
			  
		}


}
