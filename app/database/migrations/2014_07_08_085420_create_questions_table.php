<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questions', function($table)
		{
			$table->increments('id');
			$table->string('question');	
			$table->enum('question_type', array('open', 'single', 'multiple', 'check-in'));
			$table->text('answer');
			$table->enum('recipient_type', array('public', 'team'));
			$table->string('recipient');	
			$table->integer('num_views');
			$table->enum('status', array('publish', 'draft'));		
			$table->integer('created_by');
			$table->enum('is_active', array('n', 'y'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questions');
	}

}
