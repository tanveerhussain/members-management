<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email_address')->unique();
			$table->string('username')->unique();
			$table->string('password');
			$table->string('picture');
			$table->string('address');
			$table->string('city');
			$table->string('state');
			$table->string('zipcode');
			$table->string('country');
			$table->text('biography');
			$table->enum('profile_type', array('public', 'private'));
			$table->string('activation_token');
			$table->date('token_expiry');
			$table->enum('is_active', array('n', 'y'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
