<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeaminvitationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teaminvitations', function($table)
		{
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');	
			$table->string('email_address');	
			$table->integer('team_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teaminvitations');
	}

}
