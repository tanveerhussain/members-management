<!--Import HTML layout using extends-->
@extends('layout')

<!--Update web page title section-->
@section('title')
    Thank you
@stop


<!--Update web page content section-->
@section('content')

<div class="container">
  <header>
    <h1><a href="/" >Members Management <span>in Laravel</span></a></h1>
  </header>
  <section>
    <div id="container_demo" > 
      <div id="wrapper">
        <div id="login" class="animate form"> 
            
            @if (Session::has('flash_msg'))   		
            <h1>Congratulation</h1>
            <p>{{Session::get('flash_msg')}}</p>
            @endif
            
            @if (Session::has('error_msg'))   		
            <h1>Sorry</h1>
            <p>{{Session::get('error_msg')}}</p>
            @endif
            
            <p>&nbsp;</p>
            <p class="change_link"> Already a member ? <a href="{{URL::to('user/login')}}" class="to_register"> Go and log in </a> </p>
        </div>
      </div>
    </div>
  </section>
</div>
@stop