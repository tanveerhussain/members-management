<!--Import HTML layout using extends-->
@extends('layout') 

<!--Update web page title section--> 
@section('title')
    Forgot Password
@stop 

<!--Update web page content section--> 
@section('content')
<div class="container">
  <header>
    <h1><a href="/" >Members Management <span>in Laravel</span></a></h1>
  </header>
  <section>
    <div id="container_demo" > 
      <a class="hiddenanchor" id="tologin"></a>
      <div id="wrapper">
        <div id="login" class="animate form"> 
          <!--Create form using laravel core feature--> 
          {{ Form::open() }}
          <h1>Forgot Password</h1>
          
          <!--Check if there any problem with login details and if found any issue then display error to user--> 
          @if (Session::has('error_msg'))
          <p class="error" >{{Session::get('error_msg')}}</p>
          @endif
          
          @if (Session::has('flash_error'))
          <p >{{Session::get('flash_error')}}</p>
          @endif
          
          <p>
          	<!--Create email address field--> 
            {{ Form::label('email_address', 'Your email', array('class' => 'youmail', 'data-icon' => 'e')) }}
            {{Form::text('email_address', Input::old('email_address'), array('placeholder' => 'Email address'));}} </p>
            
              <!--Dispaly error if it is related with email address--> 
              @if($errors->has('email_address'))
              <p class="error">{{ $errors->first('email_address') }}</p>
              @endif
          
          
          <p class="login button"> 
            <!--Create submit button--> 
            {{Form::submit('Submit');}} </p>
          <p class="change_link"> Already a member ? <a href="{{URL::to('user/login')}}" class="to_register"> Go and log in </a> </p>
          <!--End form--> 
          {{ Form::close() }} </div>
      </div>
    </div>
  </section>
</div>
@stop