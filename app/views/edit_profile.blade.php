<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    Edit Profile
@stop

@section('asset_location')
<script src="{{asset('assets/js/uploader.js')}}" type="text/javascript"></script>
@stop

<!--Update web page content section-->
@section('content')
	  <div id="wrapper" class="wrapper_dashboard">
        <div id="login" class="animate form">
          <!--Create form using laravel core feature-->	
		   {{ Form::open() }}
            <h1> Edit Profile </h1>
            
            <!--Check if there any error message and if found any issue then display to user-->
           @if (Session::has('flash_error'))   		
                <p class="error" align="center">{{Session::get('flash_error')}}</p>
            @endif
            
             <!--Check if there any success message and if found any then display to user-->
           @if (Session::has('flash_msg'))   		
                <p class="f20" align="center">{{Session::get('flash_msg')}}</p>
            @endif
            
            <p>
              <!--Create first name field-->	
              {{ Form::label('first_name', 'Your first name', array('class' => 'uname', 'data-icon' => 'u')) }}
  			  {{Form::text('first_name', $user->first_name, array('placeholder' => 'First name'));}}	
            </p>
            
            <!--Dispaly error if it is related with first name-->	
            @if($errors->has('first_name'))
             <p class="error">{{ $errors->first('first_name') }}</p>
            @endif 
            
            <p>
              <!--Create last name field-->	
              {{ Form::label('last_name', 'Your last name', array('class' => 'uname', 'data-icon' => 'u')) }}
  			  {{Form::text('last_name', $user->last_name, array('placeholder' => 'Last name'));}}	
            </p>
            
             <!--Dispaly error if it is related with first name-->	
            @if($errors->has('last_name'))
             <p class="error">{{ $errors->first('last_name') }}</p>
            @endif 
            
            <p>
              <!--Create email address field-->	
              {{ Form::label('email_address', 'Your email', array('class' => 'youmail', 'data-icon' => 'e')) }}
  			  {{Form::text('email_address', $user->email_address, array('placeholder' => 'Email address'));}}	
            </p>
            
            <!--Dispaly error if it is related with Email Address-->	
            @if($errors->has('email_address'))
             <p class="error">{{ $errors->first('email_address') }}</p>
            @endif 
            
            <p>
              <!--Create username field-->	
              {{ Form::label('username', 'Your username', array('class' => 'uname', 'data-icon' => 'u')) }}
  			  {{Form::text('username', $user->username, array('placeholder' => 'Username'));}}	
            </p>
            
            <!--Dispaly error if it is related with username-->	
            @if($errors->has('username'))
             <p class="error">{{ $errors->first('username') }}</p>
            @endif 
            
            <p>
              <!--Create address field-->	
              {{ Form::label('address', 'Your address') }}
  			  {{Form::text('address', $user->address, array('class' => 'textbox', 'placeholder' => 'Address'));}}	
            </p>
            
            <!--Dispaly error if it is related with address-->	
            @if($errors->has('address'))
             <p class="error">{{ $errors->first('address') }}</p>
            @endif 
            
            <p>
              <!--Create city field-->	
              {{ Form::label('city', 'Your city') }}
  			  {{Form::text('city', $user->city, array('class' => 'textbox', 'placeholder' => 'City'));}}	
            </p>
            
            <!--Dispaly error if it is related with city-->	
            @if($errors->has('city'))
             <p class="error">{{ $errors->first('city') }}</p>
            @endif 
            
            <p>
              <!--Create state field-->	
              {{ Form::label('state', 'Your state') }}
  			  {{Form::text('state', $user->state, array('class' => 'textbox', 'placeholder' => 'State'));}}	
            </p>
            
            <!--Dispaly error if it is related with state-->	
            @if($errors->has('state'))
             <p class="error">{{ $errors->first('state') }}</p>
            @endif 
            
            <p>
              <!--Create zipcode field-->	
              {{ Form::label('zipcode', 'Your zipcode') }}
  			  {{Form::text('zipcode', $user->zipcode, array('class' => 'textbox', 'placeholder' => 'Zipcode'));}}	
            </p>
            
            <!--Dispaly error if it is related with zipcode-->	
            @if($errors->has('zipcode'))
             <p class="error">{{ $errors->first('zipcode') }}</p>
            @endif 
            
            <p>
              <!--Create country field-->	
              {{ Form::label('country', 'Your country') }}
  			  {{Form::text('country', $user->country, array('class' => 'textbox', 'placeholder' => 'Country'));}}	
            </p>
            
            <!--Dispaly error if it is related with country-->	
            @if($errors->has('country'))
             <p class="error">{{ $errors->first('country') }}</p>
            @endif 
            
            <p>
              <!--Create profile type field-->	
              {{ Form::label('profile_type', 'Your profile type', array('class' => 'uname')) }}
              {{ Form::select('profile_type', array('public' => 'Public', 'private' => 'Private'), $user->profile_type, array('class' => 'selectbox')) }}
            </p> 
            
            <p>
              <!--Create biography field-->	
              {{ Form::label('biography', 'Your biography') }}
  			  {{Form::textarea('biography', $user->biography, array('class' => 'textbox', 'placeholder' => 'Biography'));}}	
            </p>
            
            <!--Dispaly error if it is related with biography-->	
            @if($errors->has('biography'))
             <p class="error">{{ $errors->first('biography') }}</p>
            @endif   
            
            
            <p>
              <!--Create profile picture field-->	
              {{ Form::label('picture', 'Your profile picture') }}
  			  {{Form::file('picture')}}	  
            </p> 
            
            <p id="profile_picture">
            	 @if($user->picture)
                 <img src="{{ asset('assets/images/users/thumb_') }}{{ $user->picture }}" alt="{{ $user->first_name }} {{ $user->last_name }}" title="{{ $user->first_name }} {{ $user->last_name }}"  />
                 @else
                 <img src="{{ asset('assets/images/profile_picture.png') }}"  alt="{{ $user->first_name }} {{ $user->last_name }}" title="{{ $user->first_name }} {{ $user->last_name }}"  />
                 @endif
                 
            </p>        
           
            
            <p class="signin button textLeft">
              <!--Create submit button-->	
			  {{Form::submit('Edit Profile');}}
            </p>
            
           <!--End form-->	
		   {{ Form::close() }}
        </div>
        <div>&nbsp;</div>
      </div>     
@stop