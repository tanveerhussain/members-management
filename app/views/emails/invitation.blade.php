<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Members Management in Laravel</h2>
		
        <p>Dear user,</p>
        <p>One of your friend [{{$user['first_name']}} {{$user['last_name']}}] has been recommended to use our service and below is the message that [{{$user['first_name']}} {{$user['last_name']}}] has been written for you.</p>
        <p>{{$user['message']}}</p>
        <p>Please <a href="{{URL::to('/')}}" target="_blank" >click here</a> to visit the site for more information.</p>
        <p>&nbsp;</p>
        <p>Thank you, <br> <a href="{{URL::to('/')}}" target="_blank" >Members Management in Laravel</a></p>
	</body>
</html>