<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Members Management in Laravel</h2>
		
        <p>Dear {{$user['first_name']}} {{$user['last_name']}},</p>
        <p> To reset your password, please click the link below. Your changes here will update the password for Members Management in Laravel.</p>
		<p><a href="{{URL::to('user/reset')}}/{{$user['token']}}" target="_blank" >Reset Password</a></p>
        <p>&nbsp;</p>
        <p>Thank you, <br> <a href="{{URL::to('/')}}" target="_blank" >Members Management in Laravel</a></p>
	</body>
</html>
