<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    	<h2>Members Management in Laravel</h2>
        
		<h3>Congratulation</h3>
		
        <p>Dear {{$user['first_name']}} {{$user['last_name']}}, </p>
        <p>Your account has been created, please click the link below to make your account active.</p>
        <p><a href="{{URL::to('user/activation')}}/{{$user['token']}}" target="_blank" >Active Account</a></p>
        <p>&nbsp;</p>
        <p>Thank you, <br> <a href="{{URL::to('/')}}" target="_blank" >Members Management in Laravel</a></p>
        
		
	</body>
</html>
