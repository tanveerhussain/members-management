<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    {{ $question['question'] }}
@stop


<!--Update web page content section-->
@section('content')
<div id="wrapper" class="wrapper_dashboard">
  
  @if($question)
  <div class="animate form">
      <h1>{{ $question['question'] }}</h1>
      @if(Auth::user()->id <> $question['created_by'])
      {{ Form::open(array('url' => 'questions/save_response', 'files' => true)) }}
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
        <tbody>
        
        @if($question['question_type'] == 'open' or $question['question_type'] == 'check-in')
       
        @if (Session::has('flash_error'))   		
           <tr><td align="center" valign="top" style="padding-top:10px;" ><p class="error" align="center">{{Session::get('flash_error')}}</p></td></tr>
        @endif
        
        @if (Session::has('flash_msg'))   		
           <tr><td align="center" valign="top" style="padding-top:10px;" ><p class="f20" align="center">{{Session::get('flash_msg')}}</p></td></tr>
        @endif
        
        @if(count($comment) > 0)
        	{{-- */$your_response = $comment[0]->response;/* --}}
            {{-- */$control_att = 'disabled';/* --}}
            {{-- */$control_att_val = 'true';/* --}}
          @else
            {{-- */$your_response = Input::old('response');/* --}}
            {{-- */$control_att = 'placeholder';/* --}}
            {{-- */$control_att_val = 'Response';/* --}}
        @endif
        
        <tr><td align="left" valign="top">{{ Form::label('response', 'Your response') }}</td></tr>
        <tr><td align="left" valign="top">{{Form::textarea('response', $your_response, array('class' => 'textbox', $control_att => $control_att_val));}}</td></tr>
        
        @if($errors->has('response'))
   	     <tr><td align="left" valign="top" style="padding-top:10px;" ><p class="error">The response field is required.</p></td></tr>
        @endif
        
        @if($question['question_type'] == 'check-in')

        @if(count($comment) > 0)
        	{{-- */$your_location = $comment[0]->location;/* --}}
            {{-- */$control_att = 'disabled';/* --}}
            {{-- */$control_att_val = 'true';/* --}}
          @else
            {{-- */$your_location = Input::old('location');/* --}}
            {{-- */$control_att = 'placeholder';/* --}}
            {{-- */$control_att_val = 'Location';/* --}}
        @endif
        
        <tr><td align="left" valign="top" >&nbsp;</td></tr>
        <tr><td align="left" valign="top">{{ Form::label('location', 'Your location') }}</td></tr>
        <tr><td align="left" valign="top">{{Form::text('location', $your_location , array('class' => 'textbox', $control_att => $control_att_val));}}</td></tr>
        @if($errors->has('location'))
   	     <tr><td align="left" valign="top" style="padding-top:10px;" ><p class="error">The location field is required.</p></td></tr>
        @endif
        
        <tr><td align="left" valign="top" >&nbsp;</td></tr>
        @if(count($comment) > 0)
        @if($comment[0]->picture)
        <tr>
        	<td align="left" valign="top">
        		<img src="{{ asset('assets/images/questions/thumb_') }}{{ $comment[0]->picture }}" alt="" title=""  />
        	</td>
        </tr>
        @endif	
        @else
        <tr><td align="left" valign="top">{{ Form::label('picture', 'Your picture') }} {{Form::file('picture')}}</td></tr>
        @endif
        
        @if($errors->has('picture'))
   	     <tr><td align="left" valign="top" style="padding-top:10px;" ><p class="error">{{ $errors->first('picture') }}</p></td></tr>
        @endif
        
        @endif
        
        @else
        
        @if (Session::has('flash_error'))   		
           <tr><td align="center" valign="top" style="padding-top:10px;" ><p class="error" align="center">{{Session::get('flash_error')}}</p></td></tr>
        @endif
        
        @if (Session::has('flash_msg'))   		
           <tr><td align="center" valign="top" style="padding-top:10px;" ><p class="f20" align="center">{{Session::get('flash_msg')}}</p></td></tr>
        @endif
        
         <tr>
         	<td align="left" valign="top">
                <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0" >
                   @if($question['question_type'] == 'single')
                   	   {{-- */$single_response = '';/* --}}	
                   	   @if(count($comment) > 0)
                       		{{-- */$single_response = $comment[0]->response;/* --}}	
                       @endif	
                       @foreach($question['multiple_answers'] as $ans)
                        {{-- */$is_checked = '';/* --}}	
                       	@if($single_response == $ans)
                        	{{-- */$is_checked = 'true';/* --}}	
                        @endif
                        <tr>
                            <td align="left" valign="top" width="2%">{{Form::radio('single_response', $ans, $is_checked)}}</td>
                            <td align="left" valign="top" width="98%">{{ Form::label('single_response', $ans) }}</td>
                        </tr>
                        <tr><td colspan="2" height="5"></td></tr>
                        @endforeach
                        
                        @if($errors->has('single_response'))
   	     					<tr><td align="left" valign="top" colspan="2" style="padding-top:10px;" ><p class="error">This field is required.</p></td></tr>
        				@endif
                        
                    @endif 
                    
                    @if($question['question_type'] == 'multiple')
                       {{-- */$multiple_response = array();/* --}}	
                   	   @if(count($comment) > 0)
                       		{{-- */$multiple_response = explode('|||',$comment[0]->response);/* --}}	
                       @endif
                       @foreach($question['multiple_answers'] as $ans)
                        {{-- */$is_checked = '';/* --}}	
                       	@if(in_array($ans, $multiple_response))
                        	{{-- */$is_checked = 'true';/* --}}	
                        @endif
                        <tr>
                            <td align="left" valign="top" width="2%">{{Form::checkbox('multiple_response[]', $ans, $is_checked)}}</td>
                            <td align="left" valign="top" width="98%">{{ Form::label('multiple_response', $ans) }}</td>
                        </tr>
                        <tr><td colspan="2" height="5"></td></tr>
                        @endforeach
                        
                         @if($errors->has('multiple_response'))
   	     					<tr><td align="left" valign="top" colspan="2" style="padding-top:10px;" ><p class="error">This field is required.</p></td></tr>
        				@endif
                        
                    @endif 
                    
                </table>
            </td>
         </tr>
         
        @endif
        <tr><td align="left" valign="top" height="5" ></td></tr>
        <tr><td align="left" valign="top" >&nbsp;</td></tr>
        @if(count($comment) == 0)
        <tr><td align="left" valign="top"><a href="javascript:void(0);" onclick="submitfrm();" class="submit_link" >&nbsp;&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;</a></td></tr>
        <tr><td align="left" valign="top" >&nbsp;</td></tr>
        @endif
        <tr><td align="left" valign="top" >&nbsp;</td></tr>
        <tr><td align="left" valign="top" class="result_sep">&nbsp;</td></tr>
        </tbody>
      </table>
      {{ Form::hidden('question_type', $question['question_type'], array('id' => 'question_type')); }}
      {{ Form::hidden('question_id', $question['id'], array('id' => 'question_id')); }}
      	<input type="submit" name="submit" id="submit" value="submit" style="display:none"  />
      {{ Form::close() }}
      @else
       @if($question['question_type'] == 'single' or $question['question_type'] == 'multiple') 
			<script src="{{asset('assets/highcharts/highcharts.js')}}"></script>
            <script src="{{asset('assets/highcharts/exporting.js')}}"></script>
            <script>
                $(function () {
                    $('#container_chart').highcharts({
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 1,//null,
                            plotShadow: false
                        },
                        title: {
                            text: "{{ $question['question'] }}"
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.y}</b>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: {point.y}',
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        series: [{
                            type: 'pie',
                            name: 'Number of reply',
                            data: [
                                @foreach($responses as $res)
                                ["{{$res['answer']}}",   {{$res['total']}}]	,
                                @endforeach
                            ]
                        }]
                    });
                });
            
           </script>
            <div id="container_chart" style="margin: 0 auto"></div>	
       @endif 
       
       @if($question['question_type'] == 'check-in' and count($responses) > 0)
        <div id="maps">
     	<?php 
		require_once(app_path()."/includes/simpleGMapAPI.php");
		$map = new simpleGMapAPI();
		
		$map->setWidth('100%');
		$map->setHeight('500px');
		$map->setBackgroundColor('#000');
		$map->setMapDraggable(true);
		$map->setDoubleclickZoom(true);
		$map->setScrollwheelZoom(true);
		
		$map->showDefaultUI(true);
		$map->showMapTypeControl(true, 'DROPDOWN_MENU');
		$map->showNavigationControl(true, 'DEFAULT');
		$map->showScaleControl(true);
		$map->showStreetViewControl(true);
		
		$map->setZoomLevel(6); // not really needed because showMap is called in this demo with auto zoom
		$map->setInfoWindowBehaviour('SINGLE_CLOSE_ON_MAPCLICK');
		$map->setInfoWindowTrigger('CLICK');
		foreach($responses as $res){
			$picture = '';
			if($res->picture!=""){
					$picture = asset('assets/images/questions/thumb_').$res->picture;
			}
			$comment_show = '<table align="left" width="100%" cellpadding="0" cellspacing="0" border="0">';
			$comment_show .= '<tr><td colspan="2" align="left" valign="top"><b>'.$res->first_name.' '.$res->last_name.'</b></td></tr><tr><td colspan="2" height="8" align="left" valign="top"></td></tr>';
			$comment_show .= '<tr>';
			if($picture!=""){
				$comment_show .= '<td align="left" valign="top" width="17%"><img src="'.$picture.'" width="100" height="100" ></td>';
				$comment_show .= '<td align="left" valign="top" width="83%">'.$res->response.'</td>';
			}else{
				$comment_show .= '<td align="left" colspan="2" valign="top">'.$res->response.'</td>';
			}
			$comment_show .= '</tr><tr><td colspan="2" height="8" align="left" valign="top"></td></tr>';
			$comment_show .= '</table>';
			$map->addMarkerByAddress($res->location, $res->location, $comment_show, '');
		}
		
		$map->printGMapsJS();
		$map->showMap(true);
		?>
     </div>
       @endif
       
       @if($question['question_type'] == 'open' and count($responses) > 0)
       <div style="width:100%">
       <?php
	   		require_once(app_path()."/includes/words_cloud.php");
			$text = '';
			foreach($responses as $res){
				$text .= $res->response.' ';	
			}
			
			 $words = str_word_count($text, 1);
			 $word_count = count($words);
			 $unique_words = count( array_unique($words) );
			 $words_filtered = filter_stopwords($words, $stopwords);
			 $word_frequency = word_freq($words_filtered);
			 $word_c = word_cloud($word_frequency);
			 $word_cloud = $word_c[0];
			 $tags = $word_c[1];
			 echo $word_cloud;
	   ?>
       </div>
       @endif
       
     @endif 
  </div> 
  <div>&nbsp;</div> 
  @endif 
  
<script>
function submitfrm(){
	document.getElementById("submit").click();
}
</script>
  
</div>
@stop