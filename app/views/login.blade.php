<!--Import HTML layout using extends-->

@extends('layout') 

<!--Update web page title section--> 
@section('title')
    Login
@stop 

<!--Update web page content section--> 
@section('content')
<div class="container">
  <header>
    <h1><a href="/" >Members Management <span>in Laravel</span></a></h1>
  </header>
  <section>
    <div id="container_demo" > 
      <a class="hiddenanchor" id="tologin"></a>
      <div id="wrapper">
        <div id="login" class="animate form"> 
          <!--Create form using laravel core feature--> 
          {{ Form::open() }}
          <h1>Log in</h1>
          
          <!--Check if there any problem with login details and if found any issue then display error to user--> 
          @if (Session::has('flash_error'))
          <p class="error">{{Session::get('flash_error')}}</p>
          @endif
          <p>
          	<!--Create username field--> 
            {{ Form::label('username', 'Your username', array('class' => 'uname', 'data-icon' => 'u')) }}
            {{Form::text('username', Input::old('username'), array('placeholder' => 'Username'));}} </p>
            
              <!--Dispaly error if it is related with username--> 
              @if($errors->has('username'))
              <p class="error">{{ $errors->first('username') }}</p>
              @endif
          <p>
          	<!--Create Password field--> 
          	{{ Form::label('password', 'Your password', array('class' => 'youpasswd', 'data-icon' => 'p')) }}
            {{Form::password('password', array('placeholder' => 'Password'));}} </p>
            
              <!--Dispaly error if it is related with Password--> 
              @if($errors->has('password'))
              <p class="error">{{ $errors->first('password') }}</p>
              @endif
          <p class="keeplogin">
            <a href="{{URL::to('user/forgot-password')}}" >Forgot password?</a>
          </p>
          <p class="login button"> 
            <!--Create submit button--> 
            {{Form::submit('Login');}} </p>
          <p class="change_link"> Not a member yet ? <a href="{{URL::to('user/sign_up')}}" class="to_register">Join us</a> </p>
          <!--End form--> 
          {{ Form::close() }} </div>
      </div>
    </div>
  </section>
</div>
@stop