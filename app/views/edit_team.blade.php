<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    Edit Team
@stop


<!--Update web page content section-->
@section('content')
<div id="wrapper" class="wrapper_dashboard">
  <div id="login" class="animate form" >
          <!--Create form using laravel core feature-->	
           {{ Form::model($team, array('route' => array('team.update', $team['id']), 'files' => true, 'method' => 'PUT')) }}
            <h1> Edit Team </h1>
            
            <!--Check if there any error message and if found any issue then display to user-->
           @if (Session::has('flash_error'))   		
                <p class="error" align="center">{{Session::get('flash_error')}}</p>
            @endif
            
              <!--Check if there any success message and if found any then display to user-->
           @if (Session::has('flash_msg'))   		
                <p class="f20" align="center">{{Session::get('flash_msg')}}</p>
            @endif
            
            <p>
              <!--Create Title field-->	
              {{ Form::label('title', 'Title') }}
  			  {{Form::text('title', null , array('class' => 'textbox', 'placeholder' => 'Title'));}}	
            </p>            
            
             <!--Dispaly error if it is related with title-->	
            @if($errors->has('title'))
             <p class="error">{{ $errors->first('title') }}</p>
            @endif 
            
            <p>
              <!--Create description field-->	
              {{ Form::label('description', 'Description') }}
  			  {{Form::textarea('description', null, array('class' => 'textbox', 'placeholder' => 'Description'));}}	
            </p>
            
            <!--Dispaly error if it is related with description-->	
            @if($errors->has('description'))
             <p class="error">{{ $errors->first('description') }}</p>
            @endif 
            
            <div>
              <!--Create team members field-->	
              {{ Form::label('team_member', 'Team member') }}
              
              
              <table cellpadding="0" cellspacing="0" width="100%" border="0" align="left">
              	<tr><td colspan="2" height="8"></td></tr>
                @if(count($myfriends) > 0)
                 @foreach($myfriends as $rindex=>$rec)
              	<tr>
                	<td align="left" valign="middle" width="2%">{{Form::checkbox('team_member[]', $rindex, null)}}</td>
                    <td align="left" valign="top" width="98%">{{ Form::label('team_member', $rec) }}</td>
                </tr>
                @endforeach
                @else
                <tr><td colspan="2" height="8"><p class="error">No Member Found</p></td></tr>
                @endif                 
              </table>
              <div style="height:10px; clear:both"></div>
              
            </div>
            
             <!--Dispaly error if it is related with team_member-->	
            @if($errors->has('team_member'))
             <p class="error">{{ $errors->first('team_member') }}</p>
            @endif  
            
            <p>
              <!--Create first name field-->	
              {{ Form::label('first_name01', 'First Name # 1') }}
  			  {{Form::text('first_name01', null , array('class' => 'textbox', 'placeholder' => 'First Name'));}}	
            </p>            
            
             <!--Dispaly error if it is related with first_name-->	
             @if($errors->has('first_name01'))
             {{-- */$display_error = str_replace('is .', 'is not select.',$errors->first('first_name01'));/* --}}
             {{-- */$display_error = str_replace('name01', 'name # 1',$display_error);/* --}}
             <p class="error">{{ $display_error }}</p>
            @endif            
            
            <p>
              <!--Create last name field-->	
              {{ Form::label('last_name01', 'Last Name # 1') }}
  			  {{Form::text('last_name01', null , array('class' => 'textbox', 'placeholder' => 'Last Name'));}}	
            </p>            
            
             <!--Dispaly error if it is related with last_name-->	
             <!--Dispaly error if it is related with first_name-->	
             @if($errors->has('last_name01'))
             {{-- */$display_error = str_replace('is .', 'is not select.',$errors->first('last_name01'));/* --}}
             {{-- */$display_error = str_replace('name01', 'name # 1',$display_error);/* --}}
             <p class="error">{{ $display_error }}</p>
            @endif            
            
            
             <p>
              <!--Create email address field-->	
              {{ Form::label('email_address01', 'Email Address # 1') }}
  			  {{Form::text('email_address01', null , array('class' => 'textbox', 'placeholder' => 'Email Address', 'readonly' => true));}}	
            </p>            
            
             <!--Dispaly error if it is related with email_address-->	
             @if($errors->has('email_address01'))
             {{-- */$display_error = str_replace('is .', 'is not select.',$errors->first('email_address01'));/* --}}
             {{-- */$display_error = str_replace('address01', 'address # 1',$display_error);/* --}}
             <p class="error">{{ $display_error }}</p>
            @endif
            
            {{-- */$member_counter = 2;/* --}}
            <div class="new_member_list">
            	@if(count($team['new_members']) > 0)
                	@foreach($team['new_members'] as $new_members)
                          <div id="remove_{{$member_counter}}" >
                            <p style="padding-top:10px">
                              <!--Create first name field-->	
                              {{ Form::label('first_name', "First Name # $member_counter") }}
                              <input type="text" name="first_name[]" placeholder="First Name" class="textbox" value="{{ $new_members['first_name'] }}">
                            </p>            
                            <p style="padding-top:10px">
                              <!--Create last name field-->	
                              {{ Form::label('last_name', "Last Name # $member_counter") }}
                              <input type="text" name="last_name[]" placeholder="Last Name" class="textbox" value="{{ $new_members['last_name'] }}">
                            </p>            
                            
                             <p style="padding-top:10px">
                              <!--Create email address field-->	
                              {{ Form::label('email_address', "Email Address # $member_counter") }}
                              <input type="text" name="email_address[]" placeholder="Email Address" class="textbox" value="{{ $new_members['email_address'] }}" readonly="readonly">
                            </p>   
                            <p style="padding-top:10px;display:none;"><a href="javascript:void(0);" style="float:right;padding-right:35px;" onclick = "remove_member({{$member_counter}})" >Remove</a></p>			         					</div>
                         {{-- */$member_counter++;/* --}} 
                    @endforeach
                @endif
            </div>
            
            <p><a href="javascript:void(0);" onclick="add_more_members();" style="display:none" >Add more member</a></p>
            
            <p>
              <!--Create team Team Avatar field-->	
              {{ Form::label('team_avatar', 'Team avatar') }}
  			  {{Form::file('team_avatar')}}	  
            </p> 
            
              <!--Dispaly error if it is related with team_avatar-->	
            @if($errors->has('team_avatar'))
             <p class="error">{{ $errors->first('team_avatar') }}</p>
            @endif
            
            <p id="profile_picture">
            	 @if($team['team_avatar'])
                 <img src="{{ asset('assets/images/teams/thumb_') }}{{ $team['team_avatar'] }}" alt="{{ $team['title'] }}" title="{{ $team['title'] }}"  />
                 @else
                 <img src="{{ asset('assets/images/profile_picture.png') }}"  alt="{{ $team['title'] }}" title="{{ $team['title'] }}"  />
                 @endif
                 
            </p>
            
            <p class="signin button textLeft">
              <!--Create submit button-->	
			  {{Form::submit('Edit Team');}}
            </p>
            
            {{ Form::hidden('old_avatar', $team['team_avatar']); }}
            {{ Form::hidden('member_counter', $member_counter, array('id' => 'member_counter')); }}
            
           <!--End form-->	
		   {{ Form::close() }}
        </div>
</div>

<div class="new_member" style="display:none">
    <p style="padding-top:10px">
      <!--Create first name field-->	
      {{ Form::label('first_name', 'First Name # 1') }}
      <input type="text" name="first_name[]" placeholder="First Name" class="textbox" value="">
    </p>            
    <p style="padding-top:10px">
      <!--Create last name field-->	
      {{ Form::label('last_name', 'Last Name # 1') }}
      <input type="text" name="last_name[]" placeholder="Last Name" class="textbox" value="">
    </p>            
    
     <p style="padding-top:10px">
      <!--Create email address field-->	
      {{ Form::label('email_address', 'Email Address # 1') }}
      <input type="text" name="email_address[]" placeholder="Email Address" class="textbox" value="">
    </p>            
</div>

<script>
	
	function add_more_members(){
		var html = $('.new_member').html();
		var member_counter = $('#member_counter').val();
		html = replaceAll("# 1", "# "+member_counter, html);
		//html = html.replace("remove_textbox()", "remove_textbox("+member_counter+")"); 
		html = html + '<p style="padding-top:10px;"><a href="javascript:void(0);" style="float:right;padding-right:35px;" onclick = "remove_member('+member_counter+')" >Remove</a></p>';
		html = '<div id="remove_'+member_counter+'" >' + html + '</div>';
		$('.new_member_list').append(html);
		member_counter++;
		$('#member_counter').val(member_counter);
	}
	function replaceAll(find, replace, str) {
  		return str.replace(new RegExp(find, 'g'), replace);
	}
	function remove_member(id){
		
		if(id){
			var c = confirm("Are you sure, you want to delete this record?")
			if(c == true){
				$('#remove_'+id).remove();
			}
		}
		
	}
</script>
@stop