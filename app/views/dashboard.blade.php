<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    Dashboard
@stop


<!--Update web page content section-->
@section('content')
<div id="wrapper" class="wrapper_dashboard">
  <div id="login" class="animate form" style="display:none">
          <!--Create form using laravel core feature-->	
		   {{ Form::open() }}
            <h1> Search Members </h1>
            
            <!--Check if there any error message and if found any issue then display to user-->
           @if (Session::has('flash_error'))   		
                <p class="error" align="center">{{Session::get('flash_error')}}</p>
            @endif
            
            <p>
              <!--Create keyword field-->	
              {{ Form::label('keyword', 'Search keyword') }}
  			  {{Form::text('keyword', $keyword , array('class' => 'textbox', 'placeholder' => 'Keyword'));}}	
            </p>            
            
             <!--Dispaly error if it is related with keyword-->	
            @if($errors->has('keyword'))
             <p class="error">{{ $errors->first('keyword') }}</p>
            @endif  
            
            
            <p class="signin button textLeft">
              <!--Create submit button-->	
			  {{Form::submit('Search');}}
            </p>
            
           <!--End form-->	
		   {{ Form::close() }}
        </div>
  
  @if($users)
  <div class="animate form">
      <h1> Search Results </h1>
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
        @foreach($users as $value)
      	<tr>
        	<td align="left" valign="top" width="15%">
                @if($value->picture)
                <img src="{{ asset('assets/images/users/thumb_') }}{{ $value->picture }}" alt="{{ $value->first_name }} {{ $value->last_name }}" title="{{ $value->first_name }} {{ $value->last_name }}"  />
                @else
            	<img src="{{ asset('assets/images/profile_picture.png') }}" alt="{{ $value->first_name }} {{ $value->last_name }}" title="{{ $value->first_name }} {{ $value->last_name }}"  />
                @endif
            </td>
        	<td align="left" valign="top" width="85%" >
            	<table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
                	<tr><td align="left" valign="top">{{ $value->first_name }} {{ $value->last_name }}</td></tr>
                    <tr><td align="left" valign="top"><a href="mailto:{{ $value->email_address }}" >{{ $value->email_address }}</a></td></tr>
                    <tr><td align="left" valign="top" height="5"></td></tr>
                    <tr><td align="left" valign="top">{{ $value->address }} {{ $value->city }} {{ $value->state }}, {{ $value->zipcode }} {{ $value->country }}</td></tr>
                    <tr><td align="left" valign="top" height="15"></td></tr>
                    <tr><td align="left" valign="top">{{ $value->biography }}</td></tr>
                </table>
            </td>
        </tr>
        <tr><td align="left" valign="top" colspan="2" >&nbsp;</td></tr>
        <tr><td align="left" valign="top" colspan="2" class="result_sep">&nbsp;</td></tr>
        @endforeach
      </table>
  </div> 
  <div>&nbsp;</div>     
  @endif      
  
  
  <div class="animate form">
      <h1>Asked Questions</h1>
      
      @if(!empty($invited_questions))
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0" class="filter_section">
      	<tr>
        	<td align="right" valign="top" width="78%">&nbsp;</td>
        	<td align="left" valign="top" width="5%"><strong>Filter:</strong></td>
            <td align="left" valign="top" width="3%"><a href="javascript:void(0);" onclick="load_questions_listing('all');" class="filter_all selected_filter" >All</a></td>
            <td align="left" valign="top" width="5%"><a href="javascript:void(0);" onclick="load_questions_listing('asked');" class="filter_asked" >Asked</a></td>
            <td align="left" valign="top" width="5%"><a href="javascript:void(0);" onclick="load_questions_listing('created_by');" class="filter_created_by" >Created</a></td>
        </tr>
        <tr><td colspan="5" height="8"></td></tr>
      </table>
      <br />
      @endif
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0" id="questions_listing">
      	<thead>
        	<tr>
            	<th align="left" valign="top" width="50%">Question Title</th>
                <th align="left" valign="top" width="10%">Responses</th>
                <th align="left" valign="top" width="10%">Views</th>
                <th align="left" valign="top" width="15%">Created By</th>
                <th align="left" valign="top" width="15%">Created Date</th>
            </tr>
        </thead>
        <tbody>
          @if(!empty($invited_questions))
          @foreach($invited_questions as $value)
        	<tr>
            	<td align="left" valign="middle"><a href="{{ URL::to('questions/' . $value->id) }}" >{{ $value->question }}</a></td>
                <td align="left" valign="middle">{{ $value->num_responses }}</td>
                <td align="left" valign="middle">{{ $value->num_views }}</td>
                <td align="left" valign="middle">
                  @if($value->created_by_picture)
                    <img src="{{ asset('assets/images/users/avatar_') }}{{ $value->created_by_picture }}" alt="{{ $value->created_by }}" title="{{ $value->created_by }}"  />
                  @else
                    <img src="{{ asset('assets/images/avatar_profile_picture.png') }}" alt="{{ $value->created_by }}" title="{{ $value->created_by }}"  />
                  @endif
                	
                </td>
                <td align="left" valign="middle">{{ $value->created_at }}</td>
            </tr>
           @endforeach 
           @else
            <tr><td align="center" valign="top" colspan="5"><p class="error">No Record Found</p></td></tr>
           @endif 
        </tbody>
      </table>
      
  </div> 
  <div>&nbsp;</div> 
  
  <script>
  	function load_questions_listing(filter){
		$('.filter_section a').removeClass('selected_filter');
		$('.filter_'+filter).addClass('selected_filter');
		
		$('#questions_listing tbody').html('<tr><td align="center" valign="top" colspan="5"><img src="/assets/images/ajaxloader.gif" width="128" height="128" ></td></tr>');
		
		$.post("/filter-questions", {filter:filter}, function( data ) {
			$("#questions_listing tbody").html( data );
		});
		
	}
  </script>
  
</div>
@stop