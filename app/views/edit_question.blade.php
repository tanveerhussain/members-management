<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    Edit Question
@stop


<!--Update web page content section-->
@section('content')
<div id="wrapper" class="wrapper_dashboard">
 
  
  <div id="login" class="animate form" >
          <!--Create form using laravel core feature-->	
           {{ Form::model($question, array('route' => array('questions.update', $question['id']), 'files' => true, 'method' => 'PUT')) }}
            <h1> Edit Question</h1>
            
            <!--Check if there any error message and if found any issue then display to user-->
           @if (Session::has('flash_error'))   		
                <p class="error" align="center">{{Session::get('flash_error')}}</p>
            @endif
            
            <p>
              <!--Create question type field-->	
              {{ Form::label('question_type', 'Question type') }}
              {{ Form::select('question_type', array('open' => 'Open', 'single' => 'Single', 'multiple' => 'Multiple', 'check-in' => 'Check-in'), null, array('class' => 'selectbox')) }}
            </p>
            
            <p>
              <!--Create Question field-->	
              {{ Form::label('question', 'Question') }}
  			  {{Form::text('question', null , array('class' => 'textbox', 'placeholder' => 'Question'));}}	
            </p>            
            
             <!--Dispaly error if it is related with question-->	
            @if($errors->has('question'))
             <p class="error">{{ $errors->first('question') }}</p>
            @endif 
            
            {{-- */$display = 'display:block';/* --}}
            
            @if(Input::old('question_type'))
            	{{-- */$question_type = Input::old('question_type');/* --}}
            @else
            	{{-- */$question_type = $question['question_type'];/* --}}
            @endif
            
            @if($question_type == 'single' or $question_type == 'multiple')
            	{{-- */$display = 'display:none';/* --}}
            @endif
            
            <p class="check_in_and_open" style=" {{ $display }}">
              <!--Create answer field-->	
              {{ Form::label('answer', 'Answer') }}
  			  {{Form::textarea('answer', null, array('class' => 'textbox', 'placeholder' => 'Answer'));}}	
            </p>
            
            <!--Dispaly error if it is related with answer-->	
            @if($errors->has('answer'))
             <p class="error check_in_and_open">{{ str_replace('open',ucfirst($question_type),$errors->first('answer')) }}</p>
            @endif 
            
            
            {{-- */$display = 'display:none';/* --}}
            
            @if($question_type == 'single' or $question_type == 'multiple')
            	{{-- */$display = 'display:block';/* --}}
            @endif
            
            @if(Input::old('multiple_answers'))
            	{{-- */$multiple_answers = Input::old('multiple_answers');/* --}}
            @else
            	{{-- */$multiple_answers = $question['multiple_answers'];/* --}}
            @endif
            
            <div class="multiple_answers" style=" {{ $display }}">
                <p>
                  <!--Create multiple answers field-->	
                  {{ Form::label('multiple_answers', 'Answer') }}
                  <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="{{ $multiple_answers[0] }}">
                </p>
                
                @if($errors->has('multiple_answers[]'))
             		<p class="error check_in_and_open" style="padding-top:10px;">{{ str_replace('multiple answers[]', 'answer', $errors->first('multiple_answers[]')) }}</p>
            	@endif
                
                <div class="answer_place">
                 @if(count($multiple_answers) > 1)
                  @foreach($multiple_answers as $index=>$value)
                  @if($index > 0)
                  <p style="padding-top:10px;">
                	{{ Form::label('multiple_answers', 'Answer') }}
                    <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="{{ $value }}">
                  </p>  
                 @endif 
                 @endforeach  
                 @endif 
                 
                </div>
                
            	<p style="padding-top:10px;"><a href="javascript:void(0);" onclick="add_more_answer();" >Add more answer</a></p>
           </div>
            
            
            
            {{-- */$display = 'display:block';/* --}}
            
            @if(Auth::user()->id == 1)
            	 <p>
                  <!--Create recipient type field-->	
                  {{ Form::label('recipient_type', 'Recipient Type') }}
                  {{ Form::select('recipient_type', array('public' => 'Ask from Public', 'team' => 'Ask from Teams'), null, array('class' => 'selectbox')) }}
                </p>
                {{-- */$display = 'display:none';/* --}}
                
                 @if(Input::old('recipient_type'))
                    {{-- */$recipient_type = Input::old('recipient_type');/* --}}
                @else
                    {{-- */$recipient_type = $question['recipient_type'];/* --}}
                @endif
                
                @if($recipient_type == 'team')
                    {{-- */$display = 'display:block';/* --}}
                @endif
                
                
                @else
                {{ Form::hidden('recipient_type', 'team'); }}              
            @endif
            
            <p id="recipient_p" style="{{ $display }}">
              <!--Create recipient field-->	
              {{ Form::label('recipient', 'Recipient (Teams)') }}
              {{ Form::select('recipient[]', $recipient, null, array('class' => 'selectbox', 'multiple' => true)) }}
            </p>   
            
             <!--Dispaly error if it is related with recipient-->	
            @if($errors->has('recipient'))
             <p class="error">{{ $errors->first('recipient') }}</p>
            @endif  
            
            <p class="signin button textLeft">
              <!--Create submit button-->	
              <br />
              <a href="javascript:void(0);" class="submit_link" onclick="submitfrm('publish');" >&nbsp;&nbsp;&nbsp;Publish&nbsp;&nbsp;&nbsp;</a>
              <a href="javascript:void(0);" class="submit_link" onclick="submitfrm('draft');" >&nbsp;&nbsp;&nbsp;Draft&nbsp;&nbsp;&nbsp;</a>
              <input type="submit" name="submit" id="submit" value="submit" style="display:none"  />
              
            </p>
            
            {{ Form::hidden('status', $question['status'], array('id' => 'status')); }}
           <!--End form-->	
		   {{ Form::close() }}
        </div>
  
</div>
<p class="answer_html" style="display:none">
  {{ Form::label('multiple_answers', 'Answer') }}
  <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="">
</p>
<script>
	
	$(function(){
		
	   $('#question_type').change(function(e) {
		   $('.answer_place').html('');
			if(this.value == 'open' || this.value == 'check-in'){
				$('.multiple_answers').hide();
				$('.check_in_and_open').show();
			}else{				
				$('.check_in_and_open').hide();
				$('.multiple_answers').show();
			}
	   });	
	   
		
	   $('#recipient_type').change(function(e) {
			if(this.value == 'team'){
				$('#recipient_p').show();
			}else{
				$('#recipient_p').hide();
			}
	   });
	});
	
	function submitfrm(status){
		$('#status').val(status);
		document.getElementById("submit").click();
		
	}
	
	function add_more_answer(){
		var html = $('.answer_html').html();
		html = '<p style="padding-top:10px;">' + html + '</p>';
		$('.answer_place').append(html);
	}
	
</script>
@stop