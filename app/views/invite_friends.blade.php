<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    Invite Friends
@stop


<!--Update web page content section-->
@section('content')
	  <div id="wrapper" class="wrapper_dashboard">
        <div id="login" class="animate form">
          <!--Create form using laravel core feature-->	
		   {{ Form::open(array('files' => true)) }}
            <h1> Invite Friends </h1>
            
            <!--Check if there any error message and if found any issue then display to user-->
           @if (Session::has('flash_error'))   		
                <p class="error" align="center">{{Session::get('flash_error')}}</p>
            @endif
            
             <!--Check if there any success message and if found any then display to user-->
           @if (Session::has('flash_msg'))   		
                <p class="f20" align="center">{{Session::get('flash_msg')}}</p>
            @endif
            
            <p>
              <!--Create email address field-->	
              {{ Form::label('email_address', 'Email address', array('class' => 'youmail', 'data-icon' => 'e')) }}
  			  {{Form::text('email_address', Input::old('email_address'), array('placeholder' => 'Email address'));}}	
            </p>
            
            <!--Dispaly error if it is related with Email Address-->	
            @if($errors->has('email_address'))
             <p class="error">{{ $errors->first('email_address') }}</p>
            @endif 
            
             <p>
              <!--Create subject field-->	
              {{ Form::label('subject', 'Subject') }}
  			  {{Form::text('subject', Input::old('subject'), array('class' => 'textbox', 'placeholder' => 'Subject'));}}	
            </p>
            
            <!--Dispaly error if it is related with subject-->	
            @if($errors->has('subject'))
             <p class="error">{{ $errors->first('subject') }}</p>
            @endif 
            
            <p>
              <!--Create message field-->	
              {{ Form::label('message', 'Message') }}
  			  {{Form::textarea('message', Input::old('message'), array('class' => 'textbox', 'placeholder' => 'Message'));}}	
            </p>
            
            <!--Dispaly error if it is related with message-->	
            @if($errors->has('message'))
             <p class="error">{{ $errors->first('message') }}</p>
            @endif
               
             <p>
              <!--Create import csv email field-->	
              {{ Form::label('import_emails_via_csv', 'Import emails via CSV') }}
  			  {{Form::file('import_emails_via_csv')}}	  
            </p>    
            
            <!--Dispaly error if it is related with import csv email-->	
            @if($errors->has('import_emails_via_csv'))
             <p class="error">{{ $errors->first('import_emails_via_csv') }}</p>
            @endif   
            
            <p class="signin button textLeft">
              <!--Create submit button-->	
			  {{Form::submit('Invite Friends');}}
            </p>
            
           <!--End form-->	
		   {{ Form::close() }}
        </div>
      </div>     
@stop