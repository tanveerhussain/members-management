<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    Change Password
@stop


<!--Update web page content section-->
@section('content')
	  <div id="wrapper" class="wrapper_dashboard">
        <div id="login" class="animate form">
          <!--Create form using laravel core feature-->	
		   {{ Form::open() }}
            <h1> Change Password </h1>
            
            <!--Check if there any error message and if found any issue then display to user-->
           @if (Session::has('flash_error'))   		
                <p class="error" align="center">{{Session::get('flash_error')}}</p>
            @endif
            
             <!--Check if there any success message and if found any then display to user-->
           @if (Session::has('flash_msg'))   		
                <p class="f20" align="center">{{Session::get('flash_msg')}}</p>
            @endif
            
            <p>
              <!--Create current password field-->	
              {{ Form::label('current_password', 'Your current password', array('class' => 'youpasswd', 'data-icon' => 'p')) }}
  			  {{Form::password('current_password', array('placeholder' => 'Current password'));}}	
            </p>            
            
             <!--Dispaly error if it is related with Password-->	
            @if($errors->has('current_password'))
             <p class="error">{{ $errors->first('current_password') }}</p>
            @endif  
            
            <p>
              <!--Create new password field-->	
              {{ Form::label('password', 'Your new password', array('class' => 'youpasswd', 'data-icon' => 'p')) }}
  			  {{Form::password('password', array('placeholder' => 'New password'));}}	
            </p>            
            
             <!--Dispaly error if it is related with Password-->	
            @if($errors->has('password'))
             <p class="error">{{ $errors->first('password') }}</p>
            @endif  
           
            <p>
              <!--Create confirm password field-->	
              {{ Form::label('password_confirmation', 'Please confirm your password', array('class' => 'youpasswd', 'data-icon' => 'p')) }}
               {{Form::password('password_confirmation', array('placeholder' => 'Confirm password'));}}
            </p>
            
            <!--Dispaly error if it is related with Confirm Password-->	
            @if($errors->has('password_confirmation'))
             <p class="error">{{ $errors->first('password_confirmation') }}</p>
            @endif 
            
            
            <p class="signin button textLeft">
              <!--Create submit button-->	
			  {{Form::submit('Change Password');}}
            </p>
            
           <!--End form-->	
		   {{ Form::close() }}
        </div>
      </div>     
@stop