<!--Import HTML layout using extends-->

@extends('layout') 

<!--Update web page title section--> 
@section('title')
    Reset Password
@stop 

<!--Update web page content section--> 
@section('content')
<div class="container">
  <header>
    <h1><a href="/" >Members Management <span>in Laravel</span></a></h1>
  </header>
  <section>
    <div id="container_demo" > 
      <a class="hiddenanchor" id="tologin"></a>
      <div id="wrapper">
        <div id="login" class="animate form"> 
          <!--Create form using laravel core feature--> 
          {{ Form::open() }}
          <h1>Reset Password</h1>
          
          <!--Check if there any problem with login details and if found any issue then display error to user--> 
          @if (Session::has('flash_error'))
          <p class="error">{{Session::get('flash_error')}}</p>
          @endif
          <p>
          	<!--Create new password field--> 
            {{ Form::label('password', 'Your new password', array('data-icon' => 'p')) }}
            {{Form::password('password', array('placeholder' => 'Password'));}}
           </p>
            
              <!--Dispaly error if it is related with Password--> 
              @if($errors->has('password'))
              <p class="error">{{ $errors->first('password') }}</p>
              @endif
          
           <p>
              <!--Create confirm password field-->	
              {{ Form::label('password_confirmation', 'Please confirm your password', array('class' => 'youpasswd', 'data-icon' => 'p')) }}
               {{Form::password('password_confirmation', array('placeholder' => 'Confirm password'));}}
            </p>
            
            <!--Dispaly error if it is related with Confirm Password-->	
            @if($errors->has('password_confirmation'))
             <p class="error">{{ $errors->first('password_confirmation') }}</p>
            @endif  
          
          <p class="login button"> 
            <!--Create submit button--> 
            {{Form::submit('Submit');}} </p>
          	<p class="change_link"> Already a member ? <a href="{{URL::to('user/login')}}" class="to_register"> Go and log in </a> </p>
          <!--End form--> 
          {{ Form::close() }} </div>
      </div>
    </div>
  </section>
</div>
@stop