<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    Teams
@stop

<!--Update web page content section-->
@section('content')
<div id="wrapper" class="wrapper_dashboard">
  
  
   <!--Check if there any success message and if found any then display to user-->
   @if (Session::has('flash_msg'))   		
        <p class="f20" align="center">{{Session::get('flash_msg')}}</p>
    @endif
  
  @if($teams)
  <div class="animate form">
      <h1> Teams</h1>
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
        @foreach($teams as $value)
      	<tr>
        	<td align="left" valign="top" width="15%">
                @if($value->team_avatar)
                <img src="{{ asset('assets/images/teams/thumb_') }}{{ $value->team_avatar }}" alt="{{ $value->title }} {{ $value->title }}" title="{{ $value->title }} {{ $value->title }}"  />
                @else
            	<img src="{{ asset('assets/images/profile_picture.png') }}" alt="{{ $value->title }} {{ $value->title }}" title="{{ $value->title }} {{ $value->title }}"  />
                @endif
            </td>
        	<td align="left" valign="top" width="85%" >
            	<table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
                	<tr><td align="left" valign="top" class="f20"><strong>{{ $value->title }}</strong></td></tr>
                    <tr><td align="left" valign="top" height="15"></td></tr>
                    <tr><td align="left" valign="top">{{ $value->description }}</td></tr>
                    <tr><td align="left" valign="top" height="8"></td></tr>
                    <tr>
                    	<td align="left" valign="top" width="100%" >
                        	<table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
                            	<tr>
                                	<td align="left" valign="top" width="9%"><strong>Members:</strong></td>
                                    <td align="left" valign="top" width="91%">{{ $value->team_member }}</td>
                                </tr>
                                <tr><td align="left" valign="top" width="100%" colspan="2" height="8"></td></tr>
                                <tr><td align="left" valign="top" width="100%" colspan="2">
                                <a href="javascript:void(0);" onclick="confirm_leave({{ $value->id }});" >Click here to leave this team</a>
                                {{ Form::open() }}
                                    {{ Form::hidden('team_id', $value->id); }}
                                    <input type="submit" name="submit_{{ $value->id }}" id="submit_{{ $value->id }}"  value="submit" style="display:none" />
                                {{ Form::close() }}
                                </td></tr>
                            </table>    
                    </td></tr>
                </table>
            </td>
        </tr>
        <tr><td align="left" valign="top" colspan="2" >&nbsp;</td></tr>
        <tr><td align="left" valign="top" colspan="2" class="result_sep">&nbsp;</td></tr>
        @endforeach
      </table>
  </div> 
  <div>&nbsp;</div>
  @else
  	<p class="f20" align="center">No Record Found</p>     
  @endif      
</div>
<script>
	function confirm_leave(id){
		var c = confirm("Are you sure, you want to leave this team?");
		if(c == true){
			document.getElementById('submit_'+id).click();
		}
	}
</script>
@stop