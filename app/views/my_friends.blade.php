<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    My Friends
@stop


<!--Update web page content section-->
@section('content')
<div id="wrapper" class="wrapper_dashboard">
  
  @if($users)
  <div class="animate form">
      <h1> My Friends </h1>
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
        @foreach($users as $value)
      	<tr>
        	<td align="left" valign="top" width="15%">
                @if($value->picture)
                <img src="{{ asset('assets/images/users/thumb_') }}{{ $value->picture }}" alt="{{ $value->first_name }} {{ $value->last_name }}" title="{{ $value->first_name }} {{ $value->last_name }}"  />
                @else
            	<img src="{{ asset('assets/images/profile_picture.png') }}" alt="{{ $value->first_name }} {{ $value->last_name }}" title="{{ $value->first_name }} {{ $value->last_name }}"  />
                @endif
            </td>
        	<td align="left" valign="top" width="85%" >
            	<table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
                	<tr><td align="left" valign="top">{{ $value->first_name }} {{ $value->last_name }}</td></tr>
                    <tr><td align="left" valign="top"><a href="mailto:{{ $value->email_address }}" >{{ $value->email_address }}</a></td></tr>
                    <tr><td align="left" valign="top" height="5"></td></tr>
                    <tr><td align="left" valign="top">{{ $value->address }} {{ $value->city }} {{ $value->state }}, {{ $value->zipcode }} {{ $value->country }}</td></tr>
                    <tr><td align="left" valign="top" height="15"></td></tr>
                    <tr><td align="left" valign="top">{{ $value->biography }}</td></tr>
                </table>
            </td>
        </tr>
        <tr><td align="left" valign="top" colspan="2" >&nbsp;</td></tr>
        <tr><td align="left" valign="top" colspan="2" class="result_sep">&nbsp;</td></tr>
        @endforeach
      </table>
  </div> 
  <div>&nbsp;</div>   
  @else
  	<p class="f20" align="center">No Record Found</p>  
  @endif      
</div>
@stop