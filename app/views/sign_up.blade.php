<!--Import HTML layout using extends-->
@extends('layout')

<!--Update web page title section-->
@section('title')
    Sign-up
@stop


<link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="{{asset('assets/js/jquery.prettyPhoto.js')}}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
			 $(".gallery a[rel^='prettyPhoto']").prettyPhoto({
				deeplinking: false
			 });
		});
</script>


<!--Update web page content section-->
@section('content')

<div class="container">
  <header>
    <h1><a href="/" >Members Management <span>in Laravel</span></a></h1>
  </header>
  <section>
    <div id="container_demo" > 
      <div id="wrapper">
        <div id="login" class="animate form">
          <!--Create form using laravel core feature-->	
		   {{ Form::open() }}
            <h1> Sign up </h1>
            
           <!--Check if there any problem with register details and if found any issue then display error to user-->
           @if (Session::has('flash_error'))   		
                <p class="error">{{Session::get('flash_error')}}</p>
            @endif
            
            <p>
              <!--Create first name field-->	
              {{ Form::label('first_name', 'Your first name', array('class' => 'uname', 'data-icon' => 'u')) }}
  			  {{Form::text('first_name', Input::old('first_name'), array('placeholder' => 'First name'));}}	
            </p>
            
            <!--Dispaly error if it is related with first name-->	
            @if($errors->has('first_name'))
             <p class="error">{{ $errors->first('first_name') }}</p>
            @endif 
            
            <p>
              <!--Create last name field-->	
              {{ Form::label('last_name', 'Your last name', array('class' => 'uname', 'data-icon' => 'u')) }}
  			  {{Form::text('last_name', Input::old('last_name'), array('placeholder' => 'Last name'));}}	
            </p>
            
             <!--Dispaly error if it is related with first name-->	
            @if($errors->has('last_name'))
             <p class="error">{{ $errors->first('last_name') }}</p>
            @endif 
            
            <p>
              <!--Create email address field-->	
              {{ Form::label('email_address', 'Your email', array('class' => 'youmail', 'data-icon' => 'e')) }}
  			  {{Form::text('email_address', Input::old('email_address'), array('placeholder' => 'Email address'));}}	
            </p>
            
            <!--Dispaly error if it is related with Email Address-->	
            @if($errors->has('email_address'))
             <p class="error">{{ $errors->first('email_address') }}</p>
            @endif 
            
            <p>
              <!--Create username field-->	
              {{ Form::label('username', 'Your username', array('class' => 'uname', 'data-icon' => 'u')) }}
  			  {{Form::text('username', Input::old('username'), array('placeholder' => 'Username'));}}	
            </p>
            
            <!--Dispaly error if it is related with username-->	
            @if($errors->has('username'))
             <p class="error">{{ $errors->first('username') }}</p>
            @endif 
            
            <p>
              <!--Create password field-->	
              {{ Form::label('password', 'Your password', array('class' => 'youpasswd', 'data-icon' => 'p')) }}
  			  {{Form::password('password', array('placeholder' => 'Password'));}}	
            </p>            
            
             <!--Dispaly error if it is related with Password-->	
            @if($errors->has('password'))
             <p class="error">{{ $errors->first('password') }}</p>
            @endif  
           
            <p>
              <!--Create confirm password field-->	
              {{ Form::label('password_confirmation', 'Please confirm your password', array('class' => 'youpasswd', 'data-icon' => 'p')) }}
               {{Form::password('password_confirmation', array('placeholder' => 'Confirm password'));}}
            </p>
            
            <!--Dispaly error if it is related with Confirm Password-->	
            @if($errors->has('password_confirmation'))
             <p class="error">{{ $errors->first('password_confirmation') }}</p>
            @endif  
            
            <p class="gallery">
              <!--Create term and condition field-->		
              {{Form::checkbox('term_and_condition', 'y');}}
              {{ HTML::decode(Form::label('term_and_condition', 'I have read and accept the <a href="#terms_content" rel="prettyPhoto" >terms and conditions</a>')) }}
            </p>
            
             <!--Dispaly error if it is related with term and condition-->	
            @if($errors->has('term_and_condition'))
             <p class="error">{{ $errors->first('term_and_condition') }}</p>
            @endif
            
            
	
            
            <p class="signin button">
              <!--Create submit button-->	
			  {{Form::submit('Sign up');}}
            </p>
            <p class="keeplogin">
            <a href="{{URL::to('user/resend-activation-code')}}" >Resend Activation Code</a>
          </p>
            <p class="change_link"> Already a member ? <a href="{{URL::to('user/login')}}" class="to_register"> Go and log in </a> </p>
           <!--End form-->	
		   {{ Form::close() }}
        </div>
      </div>
    </div>
    
    <div id="terms_content" class="hide">
                <h1>Terms and Conditions</h1>
                <p>Your use of the PETA text message and alert service (“the Service”) is governed by the terms and conditions below. Use of the Service constitutes your acceptance of these terms, which take effect when you sign up for the Service. If you do not agree to these terms, please do not sign up for the Service.</p>
				<p>1. You must be at least 13 years old to use the Service, and if you are under 18 (in the U.S.) or under the age of majority in your province/territory of residence, your parent or guardian must have read and agreed to these terms before you subscribe.</p>
                <p>2. PETA does not charge for the Service, but your carrier’s standard messaging charges apply. You agree that you are responsible for paying your carrier’s charges to use the Service or, if you are under 18 (in the U.S.) or under the age of majority in your province/territory of residence, that you have permission to use the Service from the adult responsible for paying the carrier’s charges.</p>
                <p>3. PETA reserves the right to change these terms or cancel the Service at any time. Please check these terms on a regular basis for changes. Your continued use of the Service after changes are posted will mean that you accept the terms as modified by the posted changes.</p>
                <p>4. Periodic Messaging. The campaign/Service is compatible with most handsets. Supported carriers in the U.S. include ACS Wireless, Alltel (part of Verizon Wireless), Appalachian Wireless, AT&T, Bluegrass Cellular, Boost (iDEN), Boost Unlimited (CDMA), Cellcom, Cellular One from Dobson (part of AT&T), Cellular One of East Central Illinois, Cellular South, Centennial Wireless, Cincinnati Bell, GCI Communications, Illinois Valley Cellular, Immix Wireless, Inland Cellular, Nex-Tech Wireless, Nextel (part of Sprint), nTelos Wireless, Revol Wireless, Sprint, Thumb Cellular, T-Mobile, United Wireless, U.S. Cellular, Verizon Wireless, Virgin Mobile USA, and West Central Wireless.</p>
            </div>
    
  </section>
</div>
@stop