<!--Import HTML layout using extends-->
@extends('layout2')

<!--Update web page title section-->
@section('title')
    My Questions
@stop

@section('asset_location')
    <link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="{{asset('assets/js/jquery.prettyPhoto.js')}}" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                 $(".gallery a[rel^='prettyPhoto']").prettyPhoto({
                    deeplinking: false,
                    default_width: '80%',
                    default_height: '80%'
                 });
            });
    </script>
@stop


<!--Update web page content section-->
@section('content')
<div id="wrapper" class="wrapper_dashboard">
  {{-- */$display = 'display:none';/* --}}
  @if (Session::has('flash_error'))
  	{{-- */$display = '';/* --}}
  @endif
 
  
  @if(count($errors) > 0)
  	{{-- */$display = '';/* --}}
  @endif
  
  @if(empty($questions))
  	{{-- */$display = '';/* --}}
  @endif
  
  {{-- */$answer_counter = 4;/* --}}
  <div id="login" class="animate form" style=" {{ $display }}">
          <!--Create form using laravel core feature-->	
		   {{ Form::open(array('files' => true)) }}
            <h1> Ask a Question </h1>
            
            <!--Check if there any error message and if found any issue then display to user-->
           @if (Session::has('flash_error'))   		
                <p class="error" align="center">{{Session::get('flash_error')}}</p>
            @endif
            
            <p>
              <!--Create question type field-->	
              {{ Form::label('question_type', 'Question type') }}
              {{ Form::select('question_type', array('open' => 'Open', 'single' => 'Single', 'multiple' => 'Multiple', 'check-in' => 'Check-in'), Input::old('question_type'), array('class' => 'selectbox')) }}
            </p>
            
            <p>
              <!--Create Question field-->	
              {{ Form::label('question', 'Question') }}
  			  {{Form::text('question', Input::old('question') , array('class' => 'textbox', 'placeholder' => 'Question'));}}	
            </p>            
            
             <!--Dispaly error if it is related with question-->	
            @if($errors->has('question'))
             <p class="error">{{ $errors->first('question') }}</p>
            @endif 
            
            
            
            {{-- */$display = 'display:none';/* --}}
            
            @if(Input::old('question_type') == 'single' or Input::old('question_type') == 'multiple')
            	{{-- */$display = 'display:block';/* --}}
            @endif
            <div class="multiple_answers" style=" {{ $display }}">
                <p>
                  <!--Create multiple answers field-->	
                  {{ Form::label('multiple_answers', 'Answer # 1') }}
                  <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="{{ Input::old('multiple_answers')[0] }}">
                </p>
                
                <p style="padding-top:10px;">
                  <!--Create multiple answers field-->	
                  {{ Form::label('multiple_answers', 'Answer # 2') }}
                  <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="{{ Input::old('multiple_answers')[1] }}">
                </p>
                
                <p style="padding-top:10px;">
                  <!--Create multiple answers field-->	
                  {{ Form::label('multiple_answers', 'Answer # 3') }}
                  <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="{{ Input::old('multiple_answers')[2] }}">
                </p>
                
                @if($errors->has('multiple_answers[]'))
             		<p class="error multiple_answers" style="padding-top:10px;">{{ str_replace('multiple answers[]', 'answer', $errors->first('multiple_answers[]')) }}</p>
            	@endif
                
                <div class="answer_place">
                 @if(count(Input::old('multiple_answers')) > 3)
                  @foreach(Input::old('multiple_answers') as $index=>$value)
                  @if($index > 2)
                  {{-- */$sindex = $index+1;/* --}} 
                  <p style="padding-top:10px;" id="remove_{{$sindex}}">
                	{{ Form::label('multiple_answers', "Answer # $sindex") }}
                    <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="{{ $value }}">
                    <a href="javascript:void(0);" onclick="remove_textbox({{$sindex}})" ><img src="{{ asset('assets/images/remove.png') }}" alt="" title=""  /></a>
                  </p> 
                  {{-- */$answer_counter++;/* --}} 
                 @endif 
                 @endforeach  
                 @endif 
                </div>
                
            	<p style="padding-top:10px;"><a href="javascript:void(0);" onclick="add_more_answer();" >Add more answer</a></p>
           </div>     
            
            
            
            {{-- */$display = 'display:block';/* --}}
            
            @if(Auth::user()->id == 1)
            	 <p>
                  <!--Create recipient type field-->	
                  {{ Form::label('recipient_type', 'Recipient Type') }}
                  {{ Form::select('recipient_type', array('public' => 'Ask from Public', 'team' => 'Ask from Teams'), Input::old('recipient_type'), array('class' => 'selectbox')) }}
                </p>
                {{-- */$display = 'display:none';/* --}}
                @if(Input::old('recipient_type') == 'team')
                    {{-- */$display = 'display:block';/* --}}
                @endif
                
                
                @else
                {{ Form::hidden('recipient_type', 'team'); }}              
            @endif
            
            <div id="recipient_p" style="{{ $display }}">
              <!--Create recipient field-->	
              {{ Form::label('recipient', 'Recipient (Teams)') }}
              
              <table cellpadding="0" cellspacing="0" width="100%" border="0" align="left">
              	<tr><td colspan="2" height="8"></td></tr>
                @if(count($recipient) > 0)
                 @foreach($recipient as $rindex=>$rec)
              	<tr>
                	<td align="left" valign="middle" width="2%">{{Form::checkbox('recipient[]', $rindex)}}</td>
                    <td align="left" valign="top" width="98%">{{ Form::label('multiple_response', $rec) }}</td>
                </tr>
                @endforeach
                @else
                <tr><td colspan="2" height="8"><p class="error">No Team Found</p></td></tr>
                @endif                 
              </table>
              <div style="height:10px; clear:both"></div>
            </div>   
            
             <!--Dispaly error if it is related with recipient-->	
            @if($errors->has('recipient'))
             <p class="error">{{ $errors->first('recipient') }}</p>
            @endif  
            
            <p class="signin button textLeft">
              <!--Create submit button-->	
              <br />
			  <a href="javascript:void(0);" class="submit_link" onclick="submitfrm('publish');" >&nbsp;&nbsp;&nbsp;Publish&nbsp;&nbsp;&nbsp;</a>
              <a href="javascript:void(0);" class="submit_link" onclick="submitfrm('draft');" >&nbsp;&nbsp;&nbsp;Draft&nbsp;&nbsp;&nbsp;</a>
              <a href="javascript:void(0);" class="submit_link" onclick="make_preview();"  >&nbsp;&nbsp;&nbsp;Preview&nbsp;&nbsp;&nbsp;</a>
              <input type="submit" name="submit" id="submit" value="submit" style="display:none"  />
            </p>
            {{ Form::hidden('status', 'publish', array('id' => 'status')); }}
            {{ Form::hidden('answer_counter', $answer_counter, array('id' => 'answer_counter')); }}
           <!--End form-->	
		   {{ Form::close() }}
           
        </div>
 
 <p class="answer_html" style="display:none">
  {{ Form::label('multiple_answers', 'Answer #0') }}
  <input type="text" name="multiple_answers[]" placeholder="Answer" class="textbox" value="">
  <a href="javascript:void(0);" onclick="remove_textbox()" ><img src="{{ asset('assets/images/remove.png') }}" alt="" title=""  /></a>
</p>


<p class="gallery hide" >
     <a href="#preview_content" id="preview_link" rel="prettyPhoto" >Preview Link</a>
</p>  


   <!--Check if there any success message and if found any then display to user-->
   @if (Session::has('flash_msg'))   		
        <p class="f20" align="center">{{Session::get('flash_msg')}}</p>
    @endif
  
  <a name="preview"  /></a>
  @if($questions)
  <div class="animate form">
      <h1> My Questions <a href="javascript:void(0);" onclick='$( "#login" ).toggle(500);' class="team_link"  >Ask a Question</a></h1>
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0" id="questions_list">
        <tbody>
        @foreach($questions as $value)
      	<tr>
        	<td align="left" valign="top">
            	<table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
                	<tr><td align="left" valign="top" class="f20"><strong>{{ $value->question }}</strong></td></tr>
                    <tr><td align="left" valign="top" height="15"></td></tr>
                    <tr>
                    	<td align="left" valign="top" width="100%" >
                        	<table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
                               @if($value->question_type == 'single' or $value->question_type == 'multiple')
                                <tr>
                                	<td align="right" valign="top" width="13%"><strong>Answer:&nbsp;&nbsp;&nbsp;</strong></td>
                                    <td align="left" valign="top" width="87%">{{ nl2br($value->answer) }}</td>
                                </tr>
                               @endif 
                                <tr>
                                	<td align="right" valign="top" width="13%"><strong>Question Type:&nbsp;&nbsp;&nbsp;</strong></td>
                                    <td align="left" valign="top" width="87%">{{ ucfirst($value->question_type) }}</td>
                                </tr>
                                
                            	<tr>
                                	<td align="right" valign="top" width="13%"><strong>Recipient:&nbsp;&nbsp;&nbsp;</strong></td>
                                    <td align="left" valign="top" width="87%">{{ $value->recipient }}</td>
                                </tr>
                                <tr>
                                	<td align="right" valign="top" width="13%"><strong>Status:&nbsp;&nbsp;&nbsp;</strong></td>
                                    <td align="left" valign="top" width="87%">{{ ucfirst($value->status) }}</td>
                                </tr>
                                
                                 <tr>
                                	<td align="right" valign="top" width="13%"><strong>Asked Date:&nbsp;&nbsp;&nbsp;</strong></td>
                                    <td align="left" valign="top" width="87%">{{ ucfirst($value->created_at) }}</td>
                                </tr>
                                
                                <tr><td align="left" valign="top" width="100%" colspan="2" height="8"></td></tr>
                                <tr><td align="left" valign="top" width="100%" colspan="2">
                                <a href="javascript:void(0);" onclick="remove_this_question({{$value->id}});" >Click here to remove</a>
                                </td></tr>
                            </table>    
                    </td></tr>
                </table>
            </td>
        </tr>
        <tr><td align="left" valign="top" >&nbsp;</td></tr>
        <tr><td align="left" valign="top" class="result_sep">&nbsp;</td></tr>
        @endforeach
        </tbody>
      </table>
  </div> 
  <div>&nbsp;</div> 
  @else
    <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0" id="questions_list">
    	<tbody>
        </tbody>
    </table>  
  @endif      
</div>
<script>
	
	$(function(){
	
	   $('#question_type').change(function(e) {
		   $('.answer_place').html('');
		   $('#answer_counter').val(4);
			if(this.value == 'open' || this.value == 'check-in'){
				$('.multiple_answers').hide();
			}else{				
				$('.multiple_answers').show();
			}
	   });
		
	   $('#recipient_type').change(function(e) {
			if(this.value == 'team'){
				$('#recipient_p').show();
			}else{
				$('#recipient_p').hide();
			}
	   });
	});
	
	function submitfrm(status){
		$('#status').val(status);
		document.getElementById("submit").click();
		
	}
	
	
	function add_more_answer(){
		var html = $('.answer_html').html();
		var answer_counter = $('#answer_counter').val();
		html = html.replace("#0", "# "+answer_counter); 
		html = html.replace("remove_textbox()", "remove_textbox("+answer_counter+")"); 
		html = '<p style="padding-top:10px;" id="remove_'+answer_counter+'" >' + html + '</p>';
		$('.answer_place').append(html);
		answer_counter++;
		$('#answer_counter').val(answer_counter);
	}
	
	function remove_textbox(rid){
		if(rid){
			var c = confirm("Are you sure, you want to delete this record?")
			if(c == true){
				$('#remove_'+rid).remove();
			}
		}
	}
	
	function remove_this_question(qid){
		if(qid){
			var c = confirm("Are you sure, you want to remove this record?")
			if(c == true){
				window.location.href = '/questions/destroy/'+qid;
			}
		}
	}
	
	
  
	
	function previewImage() {
		var preview = document.getElementById('custom_preview');
		if (document.getElementById("picture").files && document.getElementById("picture").files[0]) {
		  var reader = new FileReader();
		  reader.onload = function (e) {
			preview.setAttribute('src', e.target.result);
		  }
		  reader.readAsDataURL(document.getElementById("picture").files[0]);
		} else {
		  preview.setAttribute('src', '/assets/images/profile_picture.png');
		}
    }
	
	function make_preview(){
		
		if($('#question').val() == ''){
			alert('Please enter Question');
			return false;
		}
		
		var q_heading = $('#question').val();
		$('#q_heading').html(q_heading);
		$('#options_listing tbody').html('');
		
		if($('#question_type').val() == 'open' || $('#question_type').val() == 'check-in'){
			$('.your_response').show();
		}else{
			$('.your_response').hide();
		}
		
		if($('#question_type').val() == 'check-in'){
			$('.your_check-in').show();
		}else{
			$('.your_check-in').hide();
		}
		
		if($('#question_type').val() == 'single' || $('#question_type').val() == 'multiple'){
			$('.option_listing').show();
			var is_empty = false;
			$('#login input[name="multiple_answers[]"]').each(function() {
				
				var html = '<tr><td align="left" valign="top" width="2%">';
				if($('#question_type').val() == 'single'){
					html = html + '<input type="radio" name="opt" >';
				}else{
					html = html + '<input type="checkbox" name="opt[]" >';
				}
				if($(this).val() != ''){
					is_empty = true;
				}
				html = html + '</td><td align="left" valign="top" width="98%">' + $(this).val() + '</td></tr><tr><td colspan="2" height="5"></td></tr>';
				$('#options_listing').append(html);
			});
			
			if(is_empty == false){
				alert("Please enter answer");
				return false;
			}
				
		}else{
			$('.option_listing').hide();
		}
		
		
		
		$('#preview_link').click();
	}
	
	function make_preview_old(){
		
		var html = '<tr class="preview">';
		
		html = html + '<td align="left" valign="top"  >';
		html = html + '<table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">';
		html = html + '<tr><td align="left" valign="top" class="f20"><strong>'+$('#question').val()+'</strong></td></tr>';
		html = html + '<tr><td align="left" valign="top" height="15"></td></tr>';
		
		if($('#question_type').val() == 'open' || $('#question_type').val() == 'check-in'){
			var answer = $('#answer').val();
		}else{
			var answer = [];
			$('input[name="multiple_answers[]"]').each(function() {
				answer.push($(this).val());
			});
		}
		
		html = html + '<tr><td align="left" valign="top" width="100%" ><table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">';
		
		html = html + '<tr><td align="right" valign="top" width="13%"><strong>Answer:&nbsp;&nbsp;&nbsp;</strong></td><td align="left" valign="top" width="87%">'+answer+'</td></tr>';		
		
		html = html + '<tr><td align="right" valign="top" width="13%"><strong>Question Type:&nbsp;&nbsp;&nbsp;</strong></td><td align="left" valign="top" width="87%">'+capitalise($('#question_type').val())+'</td></tr>';		
		
		if($('#recipient_type').val() == 'public'){
			html = html + '<tr><td align="right" valign="top" width="13%"><strong>Recipient:&nbsp;&nbsp;&nbsp;</strong></td><td align="left" valign="top" width="87%">Ask from Public</td></tr>';		
		}
		
		if($('#recipient_type').val() == 'team'){
			var selected_text = []; 
			$('#recipient :selected').each(function(i, selected){ 
			  selected_text[i] = $(selected).text(); 
			});
			
			html = html + '<tr><td align="right" valign="top" width="13%"><strong>Recipient:&nbsp;&nbsp;&nbsp;</strong></td><td align="left" valign="top" width="87%">'+selected_text+'</td></tr>';		
		}
		
		
		html = html + '<tr><td align="right" valign="top" width="13%"><strong>Status:&nbsp;&nbsp;&nbsp;</strong></td><td align="left" valign="top" width="87%">Preview</td></tr>';		
		
		html = html + '</table></td></tr>';		
		html = html + '</table>';
		html = html + '</td></tr><tr class="preview"><td align="left" valign="top" >&nbsp;</td></tr><tr class="preview"><td align="left" valign="top" class="result_sep">&nbsp;</td></tr>';
		
		$('#questions_list tr.preview').remove();
		
		$('#questions_list').prepend(html);
		
		//previewImage();
		
		var aid = 'preview';
		var aTag = $("a[name='"+ aid +"']");
    	$('html,body').animate({scrollTop: aTag.offset().top},'slow');
		
		
	}
	
	function capitalise(string) {
    	return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
   }
	
</script>

<div id="preview_content" class="hide">

<div id="wrapper" class="wrapper_dashboard">

  <div class="animate form">
      <h1 id="q_heading">What is Eloquent ORM in Laravel?</h1>
      
      <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0">
        <tbody>
        
       
        <tr class="your_response"><td align="left" valign="top">{{ Form::label('response', 'Your response') }}</td></tr>
        <tr class="your_response"><td align="left" valign="top">{{Form::textarea('response', Input::old('response'), array('class' => 'textbox', 'placeholder' => 'Response'));}}</td></tr>
        
        
        <tr class="your_check-in"><td align="left" valign="top" >&nbsp;</td></tr>
        <tr class="your_check-in"><td align="left" valign="top">{{ Form::label('location', 'Your location') }}</td></tr>
        <tr class="your_check-in"><td align="left" valign="top">{{Form::text('location', Input::old('location') , array('class' => 'textbox', 'placeholder' => 'Location'));}}</td></tr>
        
        
        <tr class="your_check-in"><td align="left" valign="top" >&nbsp;</td></tr>
        <tr class="your_check-in"><td align="left" valign="top">{{ Form::label('picture', 'Your picture') }} {{Form::file('picture')}}</td></tr>
        
        <tr class="option_listing">
         	<td align="left" valign="top">
                <table align="left" border="0" width="100%" cellpadding="0" cellspacing="0" id="options_listing" class="option_listing" >
                	<tbody>
                    </tbody>
                </table>
            </td>
         </tr>
        
        <tr><td align="left" valign="top" height="5" ></td></tr>
        <tr><td align="left" valign="top" >&nbsp;</td></tr>
        <tr><td align="left" valign="top"><a href="javascript:void(0);" class="submit_link" >&nbsp;&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;</a></td></tr>
        </tbody>
      </table>
      
      
      
      
  </div> 
  <div>&nbsp;</div> 
</div>


</div>

@stop