@if(!empty($invited_questions))
@foreach($invited_questions as $value)
<tr>
  <td align="left" valign="middle"><a href="{{ URL::to('questions/' . $value->id) }}" >{{ $value->question }}</a></td>
  <td align="left" valign="middle">{{ $value->num_responses }}</td>
  <td align="left" valign="middle">{{ $value->num_views }}</td>
  <td align="left" valign="middle">
      @if($value->created_by_picture)
        <img src="{{ asset('assets/images/users/avatar_') }}{{ $value->created_by_picture }}" alt="{{ $value->created_by }}" title="{{ $value->created_by }}"  />
      @else
        <img src="{{ asset('assets/images/avatar_profile_picture.png') }}" alt="{{ $value->created_by }}" title="{{ $value->created_by }}"  />
      @endif
  </td>
  <td align="left" valign="middle">{{ $value->created_at }}</td>
</tr>
@endforeach 
@else
<tr>
  <td align="center" valign="top" colspan="5"><p class="error">No Record Found</p></td>
</tr>
@endif 