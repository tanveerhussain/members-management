<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style3.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate-custom.css')}}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>

<script src="{{asset('assets/pusher/lib/gritter/js/jquery.gritter.min.js')}}"></script>
<link href="{{asset('assets/pusher/lib/gritter/css/jquery.gritter.css')}}"rel="stylesheet" type="text/css" />
<script src="http://js.pusher.com/2.2/pusher.min.js"></script>
<script src="{{asset('assets/pusher/PusherNotifier.js')}}"></script>
<script>
    $(function() {
      //var pusher = new Pusher('7f9bf1784d5828d957b6');
	  var pusher = new Pusher('7f9bf1784d5828d957b6', { authEndpoint: "{{URL::to('questions/auth')}}" });
      var channel = pusher.subscribe("private-{{ Auth::user()->id }}");
      var notifier = new PusherNotifier(channel);
    });
</script>

@yield('asset_location')
</head>
<body>
<div class="container">
  <header>
    <h1><a href="/" >Members Management <span>in Laravel</span></a></h1>
  </header>
  <section>
    <div id="container_demo" > 
      <div id="wrapper" class="wrapper_dashboard">
		<div class="navigation" >
        	<ul id="menu">
            	<li>Welcome '{{Auth::user()->first_name}} {{Auth::user()->last_name}}'!</li>
            	<li class="link"><a href="{{URL::to('user/logout')}}">Logout</a></li>
                <li class="link">|</li>
                <li class="link">
                	<a href="javascript:void(0);">My Account</a>
                    <ul class="sub-menu">
                    	<li><a href="{{URL::to('invite-friends')}}">Invite Friends</a></li>
                        <li><a href="{{URL::to('my-friends')}}">My Friends</a></li>
                        <li><a href="{{URL::to('team')}}">My Teams</a></li>
                        <li><a href="{{URL::to('user/edit-profile')}}">Edit Profile</a></li>
                        <li><a href="{{URL::to('user/change-password')}}">Change Password</a></li>
                    </ul>
                </li>
                <li class="link">|</li>
                <li class="link"><a href="{{URL::to('questions')}}">My Questions</a></li>
                <li class="link">|</li>
                <li class="link"><a href="{{URL::to('my-teams')}}">Teams</a></li>
                <li class="link">|</li>
                <li class="link"><a href="/">Dashboard</a></li>
            </ul>
        </div>        
      </div>
      @yield('content')
    </div>
  </section>
</div>

</body>
</html>