<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Setup home page for get request
Route::get('/', 'HomeController@index');

// Setup home page for post request
Route::post('/', 'HomeController@search_members');

// Setup my friends page for get request
Route::get('/my-friends', 'HomeController@my_friends');


// Setup invite friends page for get request
Route::get('/invite-friends', 'HomeController@invite_friends');

// Setup invite friends page for post request
Route::post('/invite-friends', 'HomeController@send_invitation');


// Setup user login page for get request
Route::get('/user/login', 'UserController@login');

// Setup user login page for post request
Route::post('/user/login', 'UserController@validateUser');

// Setup user sign up page for get request
Route::get('/user/sign_up', 'UserController@sign_up');

// Setup user sign up page for post request
Route::post('/user/sign_up', 'UserController@validateUserSignUp');

// Setup user thank you page for get request
Route::get('/user/thankyou', 'UserController@thankyou');

// Setup user activation page for post request
Route::get('/user/activation/{any}', 'UserController@activation');


// Setup user forgot password page for get request
Route::get('/user/forgot-password', 'UserController@forgot_password');

// Setup user forgot password page for post request
Route::post('/user/forgot-password', 'UserController@validateForgotPassword');

// Setup user resend activation code page for get request
Route::get('/user/resend-activation-code', 'UserController@resend_activation_code');

// Setup user resend activation code page for post request
Route::post('/user/resend-activation-code', 'UserController@validateResendActivationCode');



// Setup user reset password page for get request
Route::get('/user/reset/{any}', 'UserController@reset_password');

// Setup user reset password page for post request
Route::post('/user/reset/{any}', 'UserController@update_reset_password');


// Setup user edit profile page for get request
Route::get('/user/edit-profile', 'UserController@edit_profile');

// Setup user edit profile page for post request
Route::post('/user/edit-profile', 'UserController@update_profile');

// Setup user upload profile picture page for get request
Route::post('/user/upload_profile_picture', 'UserController@upload_profile_picture');


// Setup user change password page for get request
Route::get('/user/change-password', 'UserController@change_password');

// Setup user edit change password for post request
Route::post('/user/change-password', 'UserController@update_password');


// Setup user login page for get request
Route::get('/user/logout', 'UserController@logout');

// Setup user team page for get request
Route::get('/my-teams', 'TeamController@my_teams');

// Setup user team page for post request
Route::post('/my-teams', 'TeamController@update_team');


// Register route for team
Route::resource('team', 'TeamController');

// Register route for questions
Route::resource('questions', 'QuestionsController');

// Setup save response for post request
Route::post('/questions/save_response', 'QuestionsController@save_response');

// Setup remove question for get request
Route::get('/questions/destroy/{any}', 'QuestionsController@destroy');

// Setup filter questions for post request
Route::post('/filter-questions', 'HomeController@filter_questions');


// Setup filter auth for post request
Route::post('/questions/auth', 'QuestionsController@auth');

// Setup filter auth for get request
Route::get('/questions/auth', 'QuestionsController@auth');
